﻿using Microsoft.AspNetCore.Http;
using ST.Net.API.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ST.Net.API.Models.Document
{
    public class AddFilesViewModel
    {
		[Required(ErrorMessage = LocalizationMessageCodes.IdRequired)]
		public Guid DocumentId { get; set; }
		[Required(ErrorMessage = LocalizationMessageCodes.FileRequired)]
		public ICollection<IFormFile> Files { get; set; }
    }
}
