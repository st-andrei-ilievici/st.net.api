﻿using ST.Net.API.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ST.Net.API.Models.Document
{
    public class MoveFileViewModel
    {
		[Required(ErrorMessage = LocalizationMessageCodes.IdRequired)]
		public Guid FileId { get; set; }
		[Required(ErrorMessage = LocalizationMessageCodes.Required)]
		public string NewDirectory { get; set; }
    }
}
