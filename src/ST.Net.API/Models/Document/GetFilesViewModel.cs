﻿using System;

namespace ST.Net.API.Models.Document
{
	public class GetFilesViewModel
    {
		public Guid FileId { get; set; }
		public string FileName { get; set; }
		public string FileExtension { get; set; }
    }
}
