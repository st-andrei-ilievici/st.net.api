﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ST.Net.API.Models.Document
{
	public enum ContentType
	{
		File, Folder
	}
    public class OpenFolderViewModel
    {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Extension { get; set; }
		public string Owner { get; set; }
		public ContentType Type { get; set; }
    }
}
