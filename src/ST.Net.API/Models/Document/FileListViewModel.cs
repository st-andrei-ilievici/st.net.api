﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ST.Net.API.Models.Document
{
    public class FileListViewModel
    {
		public Guid Id { get; set; }
		public string AddedById { get; set; }
		public string FileName { get; set; }
		public string FileExtension { get; set; }
		public float Version { get; set; }
		public Guid DocumentId { get; set; }
	}
}
