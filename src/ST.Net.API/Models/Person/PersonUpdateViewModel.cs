﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.Person
{
    public class PersonUpdateViewModel
    {
        [Display(Name = "FirstName")]
        [EmailAddress]
        public string Function { get; set; }
        
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "IDNP")]
        public string Idnp { get; set; }
        
        [Display(Name = "Gender")]
        public string Gender { get; set; }
    }
}
