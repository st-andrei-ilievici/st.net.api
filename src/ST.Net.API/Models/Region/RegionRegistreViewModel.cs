﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.Region
{
    public class RegionRegistreViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}
