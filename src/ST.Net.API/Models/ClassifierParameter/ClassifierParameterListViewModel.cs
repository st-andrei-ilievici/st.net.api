﻿using System;
using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Classifier
{
    public class ClassifierParameterListViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.TitleRequired)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
        
        [Display(Name = "Order")]
        public int Order { get; set; }

    }
}
