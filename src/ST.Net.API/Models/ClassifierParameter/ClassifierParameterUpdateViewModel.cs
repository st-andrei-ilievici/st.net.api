﻿using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Classifier
{
    public class ClassifierParameterUpdateViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.TitleRequired)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }

        public bool IsVisible { get; set; }

        public bool IsDeleted { get; set; }
    }
}
