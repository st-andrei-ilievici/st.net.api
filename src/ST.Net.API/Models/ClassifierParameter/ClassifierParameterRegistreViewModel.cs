﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Classifier
{
    public class ClassifierParameterRegistreViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.TitleRequired)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Comment")]
        public string Comment { get; set; }
    }
}
