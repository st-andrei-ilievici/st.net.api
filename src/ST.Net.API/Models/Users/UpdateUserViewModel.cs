﻿using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Users
{
    public class UpdateUserViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.EmailRequired)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = LocalizationMessageCodes.EmailIsNotValid)]
        public string Email { get; set; }
        
        [Required(ErrorMessage = LocalizationMessageCodes.PasswordRequired)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
    }
}
