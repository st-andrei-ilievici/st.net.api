﻿using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Users
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.EmailRequired)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = LocalizationMessageCodes.EmailIsNotValid)]
        public string Email { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.PasswordRequired)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
