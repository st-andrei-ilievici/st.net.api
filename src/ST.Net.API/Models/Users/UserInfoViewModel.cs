﻿using System;

namespace ST.Net.API.Models.Users
{
    public class UserInfoViewModel
    {
        public string Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }
        
        public DateTime? BirthDate { get; set; }
    }
}
