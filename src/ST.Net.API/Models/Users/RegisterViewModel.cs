﻿using System;
using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Users
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.FirstNameRequired)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.LastNameRequired)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.BirthdayRequired)]
        [Display(Name = "Birthday")]
        [DataType(DataType.DateTime)]
        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.EmailRequired)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = LocalizationMessageCodes.EmailIsNotValid)]
        public string Email { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.PasswordRequired)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required(ErrorMessage = LocalizationMessageCodes.ConfirmPasswordRequired)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = LocalizationMessageCodes.ConfirmPasswordDoNotMatch)]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.PhoneNumberRequired)]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
