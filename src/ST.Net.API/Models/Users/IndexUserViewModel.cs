﻿using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Users
{
    public class IndexUserViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.FullNameRequired)]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.EmailRequired)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = LocalizationMessageCodes.EmailIsNotValid)]
        public string Email { get; set; }
    }
}
