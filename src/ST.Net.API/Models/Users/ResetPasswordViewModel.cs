﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Users
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = LocalizationMessageCodes.EmailRequired)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = LocalizationMessageCodes.EmailIsNotValid)]
        public string Email { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.PasswordRequired)]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.ConfirmPasswordRequired)]
        [Display(Name = "Confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = LocalizationMessageCodes.ConfirmPasswordDoNotMatch)]
        public string ConfirmPassword { get; set; }
    }
}
