﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.ReportComplaintParty
{
    public class ReportComplaintPartyRegistreViewModel
    {
        public Guid ReportComplaintId { get; set; }

        public Guid PersonId { get; set; }
        
        //public Guid ReportComplaintPartyTypeId { get; set; }

        public string PartyComments { get; set; }

        public bool GetFreeJudAssistance { get; set; }
    }
}
