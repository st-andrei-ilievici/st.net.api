﻿using System;

namespace ST.Net.API.Models.ReportComplaintParty
{
    public class ReportComplaintPartyListViewModel
    {
        public Guid Id { get; set; }

        public Guid ReportComplaintId { get; set; }

        public string PersonLastName { get; set; }

        public string PersonFirstName { get; set; }

        public string PersonIdnp { get; set; }
    }
}
