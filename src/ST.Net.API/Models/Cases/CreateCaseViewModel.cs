﻿using Microsoft.AspNetCore.Http;
using ST.Net.API.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.Cases
{
	public class CreateCaseViewModel
    {
		[Required(ErrorMessage = LocalizationMessageCodes.NameRequired)]
		public string Name { get; set; }
		public Guid ClassifierId { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public bool IsPublic { get; set; }
		public int MaxYears { get; set; }
		public virtual IDictionary<string, bool> CompatibleJudges { get; set; }
		public string DocumentName { get; set; }
		public string DocumentDescription { get; set; }
		public string DocumentKeywords { get; set; }
		public virtual ICollection<IFormFile> DocumentFiles { get; set; }
	}
}
