﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.ReportComplaint
{
    public class ReportComplaintRegistreViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}
