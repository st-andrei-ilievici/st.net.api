﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.Court
{
    public class ClassifierListViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}
