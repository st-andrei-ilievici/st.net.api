﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.Models.Court
{
    public class CourtUpdateViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
    }
}
