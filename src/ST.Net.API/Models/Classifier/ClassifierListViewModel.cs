﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ST.Net.API.Core.Helpers;

namespace ST.Net.API.Models.Classifier
{
    public class ClassifierListViewModel
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = LocalizationMessageCodes.TitleRequired)]
        [Display(Name = "Title")]
        public string Title { get; set; }
        
        [Display(Name = "Parameters")]
        public List<ClassifierParameterListViewModel> Parameters { get; set; }
    }
}
