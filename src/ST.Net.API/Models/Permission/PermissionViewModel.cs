﻿using System.Collections.Generic;

namespace ST.Net.API.Models.Permission
{
    public class PermissionViewModel
    {
        public PermissionViewModel()
        {
            CheckedPermissions = new Dictionary<string, bool>();
        }

        public IDictionary<string, bool> CheckedPermissions { get; set; }
    }
}
