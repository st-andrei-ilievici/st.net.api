﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using ST.Net.API.Core.EntityFramework;
using Microsoft.Extensions.Configuration;

namespace ST.Net.API.Extensions
{
    public static class ApplicationBuilderExtension
    {
        /// <summary>
        /// Setup API DataBase Migrations
        /// </summary>
        /// <param name="app"></param>
        /// <returns></returns>
        public static IApplicationBuilder SetupMigrations(this IApplicationBuilder app)
        {
            try
            {
                var serviceScope = app.ApplicationServices
                    .GetRequiredService<IServiceScopeFactory>()
                    .CreateScope();

                serviceScope.ServiceProvider.GetService<DataBaseContext>().Database.Migrate();
            }
            catch (Exception exception)
            {
                Debug.WriteLine(exception.Message);
            }
            return app;
        }

        public static IApplicationBuilder AddDevelopmentMiddlewares(this IApplicationBuilder app, IHostingEnvironment env, IConfigurationRoot configuration)
        {
            var hostingEnvironment = app.ApplicationServices.GetRequiredService<IHostingEnvironment>();
            var loggerFactory = app.ApplicationServices.GetRequiredService<ILoggerFactory>();

            if (hostingEnvironment.IsDevelopment())
            {
                //Setup API DataBase Migrations
                app.SetupMigrations();

                loggerFactory.AddConsole(configuration.GetSection("Logging"));
                loggerFactory.AddDebug();

                new ContextSeeder(app).Process();
            }

            app.UseDeveloperExceptionPage();

            env.ConfigureNLog("nlog.config");
            loggerFactory.AddNLog();

            return app;
        }
    }
}
