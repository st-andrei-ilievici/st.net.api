﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ST.BaseRepository;
using ST.Net.API.Controllers;
using ST.Net.API.Core.Controllers;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Security;
using ST.Net.API.Core.Services.Abstract;
using ST.Net.API.Core.Services.Concret;
using ST.UserIdentity;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.PlatformAbstractions;
using ST.DMS;

namespace ST.Net.API.Extensions
{
    /// <summary>
    /// Extension of <see cref="IServiceCollection"/> type.
    /// </summary>
    public static class ServiceCollectionExtension
    {
        /// <summary>
        /// Register API Services
        /// </summary>
        public static IServiceCollection RegistreApiServices(this IServiceCollection services)
        {
            //Base Controllers
            services.AddTransient(typeof(ICreateController<,,>), typeof(CreateController<,,>));
            services.AddTransient(typeof(IReadController<,,>), typeof(ReadController<,,>));
            services.AddTransient(typeof(IDeleteController<,,>), typeof(DeleteController<,,>));
            services.AddTransient(typeof(IUpdateController<,,>), typeof(UpdateController<,,>));

            //Services
            services.AddTransient<IUserService, UserService>();

            return services;
        }

        /// <summary>
        /// Register ST.Framework repositpries
        /// </summary>
        public static IServiceCollection RegistreApiRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<DataBaseContext>, Repository<DataBaseContext>>();
            services.AddScoped<IPermissionRepository<DataBaseContext>, PermissionRepository<DataBaseContext>>();
            services.AddScoped<IUserRepository<DataBaseContext>, UserRepository<DataBaseContext>>();
            services.AddScoped<IDocumentRepository<DataBaseContext>, DocumentRepository<DataBaseContext>>();
            return services;
        }

        /// <summary>
        /// Register the Swagger generator, defining one or more Swagger documents
        /// </summary>
        public static IServiceCollection RegistreApiSwagger(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddSwaggerGen(opt =>
            {
                opt.SwaggerDoc("doc", new Info
                {
                    Title = "PIGD WebAPI",
                    TermsOfService = "NA",
                    Contact = new Contact() { Name = "Soft-Tehnica SRL" }
                });

                //Set the comments path for the swagger json and ui.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "api.documentation.xml");

                if (File.Exists(xmlPath))
                    opt.IncludeXmlComments(xmlPath);

                opt.IgnoreObsoleteProperties();
                opt.IgnoreObsoleteActions();
                opt.DescribeAllEnumsAsStrings();

                opt.DescribeAllEnumsAsStrings();
                opt.DescribeStringEnumsInCamelCase();

                opt.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Type = "oauth2",
                    Flow = "implicit",
                    AuthorizationUrl = configuration["IdentityServer:Authority"] + "/connect/authorize",

                    Scopes = new Dictionary<string, string>
                    {
                        {configuration["IdentityServer:ApiName"], "API"}
                    },
                    TokenUrl = configuration["IdentityServer:Authority"] + "/connect/token",
                });

            });

            return services;
        }

        /// <summary>
        /// Register SecurityPolicy Services
        /// </summary>
        public static IServiceCollection RegistreSecurityPolicy(this IServiceCollection services)
        {
            SecurityPolicyContext.RegisterType<CourtsController>();
            SecurityPolicyContext.RegisterType<PermissionsController>();
            SecurityPolicyContext.RegisterType<PersonsController>();
            SecurityPolicyContext.RegisterType<RegionsController>();
            SecurityPolicyContext.RegisterType<ReportComplaintPartiesController>();
            SecurityPolicyContext.RegisterType<UsersController>();

            return services;
        }
    }
}