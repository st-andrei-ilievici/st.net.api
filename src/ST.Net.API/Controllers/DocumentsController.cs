﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.DMS;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.Net.API.Models.Document;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ST.Net.API.Controllers
{
	/// <summary>
	/// Controller for managing Documents and Files
	/// </summary>
	[Produces("application/json")]
	[Route("api/[controller]")]
	public class DocumentsController : Controller
    {
		private readonly IReadController<DocumentsController, FileVersion, FileListViewModel> _readController;
		private readonly IDocumentRepository<DataBaseContext> _documentRepository;
		private readonly ILogger _logger;
		public DocumentsController(IDocumentRepository<DataBaseContext> documentRepository, 
			ILoggerFactory loggerFactory,
			IReadController<DocumentsController,FileVersion,FileListViewModel> readController)
		{
			_documentRepository = documentRepository;
			_logger = loggerFactory.CreateLogger<CasesController>();
			_readController = readController;
		}

		/// <summary>
		/// Get list of files by pages. 
		/// </summary>
		/// <param name="page">Number of Page</param>
		/// <param name="pageSize">Number of elements on pages, default is 10</param>
		/// <returns>List of <see cref="FileVersion"/> objects</returns>
		[HttpGet("getfiles")]
		public IActionResult GetFiles(int page, int pageSize = 10)
		{
			return _readController.GetByPage(page, pageSize);
		}

		/// <summary>
		/// Download a physical file using <see cref="FileVersion"/> Id
		/// </summary>
		/// <param name="fileId">Id of the file</param>
		/// <returns>Byte array of the found file</returns>
		[HttpGet("download")]
		public JsonResult Download(Guid fileId)
		{
			FileVersion fileVersion = null;
			try
			{
				fileVersion = _documentRepository.GetSingle<FileVersion>(fileId);
			}catch(Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "File not found").ToString());
				return this.ApiBadInputDataResult();
			}
			if(fileVersion!=null)
			{
				var filePath = fileVersion.Path;
				var serverPath = _documentRepository.GetServerPath();
				if (System.IO.File.Exists(Path.Combine(serverPath,filePath)))
				{
					var fileContent = System.IO.File.ReadAllBytes(Path.Combine(serverPath, filePath));
					_logger.LogInformation(new Logger.Log(fileVersion, "Entity of type " + typeof(FileVersion) + " downloaded.").ToString());
					return this.ApiSuccesResult(new SearchResult { Results = new List<FileContentResult>{ new FileContentResult(fileContent, "application/octet-stream") } });
				}
			}
			_logger.LogError(new Logger.Log(fileId, "File not found").ToString());
			return this.ApiCustomErrorResult(LocalizationMessageCodes.FileNotFound);
		}

		/// <summary>
		/// Get <see cref="FileVersion"/> list related to selected <see cref="Document"/> Id
		/// </summary>
		/// <param name="documentId">Id of the selected<see cref="Document"/></param>
		/// <returns>List of <see cref="FileVersion"/> objects</returns>
		[HttpGet("getdocumentfiles")]
		public JsonResult GetDocumentFiles(Guid documentId)
		{
			IEnumerable<GetFilesViewModel> result;
			Document document = null;
			try
			{
				document = _documentRepository.GetSingle<Document>(documentId);
			}catch(Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "File not renamed").ToString());
				return this.ApiCustomErrorResult(LocalizationMessageCodes.ReadError);
			}
			if (document == null)
			{
				_logger.LogError(new Logger.Log(documentId, "Document not found.").ToString());
				return this.ApiBadInputDataResult();
			}
			var content = _documentRepository.OpenFolder(document.FolderId);
			result = content.FileVersions.Select(x => new GetFilesViewModel
			{
				FileExtension = x.FileExtension,
				FileId = x.Id,
				FileName = x.FileName
			});

			_logger.LogInformation(new Logger.Log(result, "Entity of type " + typeof(GetFilesViewModel) + " saved.").ToString());
			return this.ApiSuccesResult(new SearchResult { Results = result });
		}

		/// <summary>
		/// List of <see cref="FileVersion"/> and <see cref="Folder"/> objects found in selected <see cref="Folder"/> Id.
		/// If <see cref="Folder"/> Id is null, the resulting objects will be from root path
		/// </summary>
		/// <param name="folderId">Selected <see cref="Folder"/> Id</param>
		/// <returns>List of <see cref="Folder"/>s and <see cref="FileVersion"/>s from selected <see cref="Folder"/></returns>
		[HttpGet("openfolder")]
		public JsonResult OpenFolder(Guid?  folderId)
		{
			var content = _documentRepository.OpenFolder(folderId);
			var result = new List<OpenFolderViewModel>();

			result.AddRange(content.FileVersions.Select(x => new OpenFolderViewModel
			{
				Id = x.Id,
				Extension = x.FileExtension,
				Name = x.FileName,
				Owner = x.AddedById,
				Type = ContentType.File
			}));

			result.AddRange(content.Folders.Select(x => new OpenFolderViewModel
			{
				Id = x.Id,
				Name = x.Name,
				Owner = x.AddedById,
				Type = ContentType.Folder
			}));

			_logger.LogInformation(new Logger.Log(result, "Entity of type " + typeof(OpenFolderViewModel) + " saved.").ToString());
			return this.ApiSuccesResult(new SearchResult { Results = result });
		}

		/// <summary>
		/// Rename a selected File
		/// </summary>
		/// <param name="model"><see cref="RenameFileViewModel"/> object containing selected <see cref="FileVersion"/> Id and new File name</param>
		/// <returns>New path of the renamed file</returns>
		[HttpPost("renamefile")]
		public JsonResult Rename(RenameFileViewModel model)
		{
			if (model == null) return this.ApiBadInputDataResult();
			if (!ModelState.IsValid) return this.ApiBadInputDataResult();
			string result;
			try
			{
				result = _documentRepository.RenameFile(model.FileId, model.NewName);
			}catch( Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "File not renamed").ToString());
				return this.ApiCustomErrorResult(LocalizationMessageCodes.UpdateError);
			}
			if(!string.IsNullOrEmpty(result))
			{
				_logger.LogInformation(new Logger.Log(result, "File succesfully renamed").ToString());
				return this.ApiSuccesResult(new SearchResult { Results = new string[] { result} });
			}
			_logger.LogError(new Logger.Log(model, "File not saved").ToString());
			return this.ApiErrorsResult();
		}

		/// <summary>
		/// Move a <see cref="FileVersion"/> to another <see cref="Folder"/>
		/// </summary>
		/// <param name="model">Model containing selected <see cref="FileVersion"/> Id and new Directory path</param>
		/// <returns>New path of the moved file</returns>
		[HttpPost("movefile")]
		public JsonResult MoveFile(MoveFileViewModel model)
		{
			if (model == null) return this.ApiBadInputDataResult();
			if (!ModelState.IsValid) return this.ApiBadInputDataResult();
			string result;
			try
			{
				result = _documentRepository.MoveFileTo(model.FileId, model.NewDirectory);
			}catch(Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "File already exists").ToString());
				return this.ApiCustomErrorResult(LocalizationMessageCodes.FileAlreadyExists);
			}
			
			if (!string.IsNullOrEmpty(result))
			{
				_logger.LogInformation(new Logger.Log(result, "File succesfully moved").ToString());
				return this.ApiSuccesResult(new SearchResult { Results = new string[] { result } });
			}
			_logger.LogError(new Logger.Log(model, "File not moved").ToString());
			return this.ApiErrorsResult();
		}

		/// <summary>
		/// Upload a new version of an existing file. Old <see cref="FileVersion"/> object is changing is state to IsDeleted. New object gets it's version incremented.
		/// </summary>
		/// <param name="model"><see cref="UpdateFileViewModel"/> object containing Id of the old <see cref="FileVersion"/> object, new uploaded file and flag determining if the change IsMajor or not</param>
		/// <returns>Path of the new Updated <see cref="FileVersion"/></returns>
		[HttpPost("updatefile")]
		public JsonResult UpdateFile(UpdateFileViewModel model)
		{
			if (model == null) return this.ApiBadInputDataResult();
			if (!ModelState.IsValid) return this.ApiBadInputDataResult();
			string result;
			try
			{
				result = _documentRepository.UpdateFile(model.FileId, model.NewFile, model.IsMajor, User.Identity.Name);
			}catch(Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "Update Failed").ToString());
				return this.ApiCustomErrorResult(LocalizationMessageCodes.UpdateError);
			}

			if(!string.IsNullOrEmpty(result))
			{
				_logger.LogInformation(new Logger.Log(result, "File succesfully updated").ToString());
				return this.ApiSuccesResult(new SearchResult { Results = new string[] { result } });
			}
			_logger.LogError(new Logger.Log(model, "File not updated").ToString());
			return this.ApiErrorsResult();
		}

		/// <summary>
		/// Add Files to an existing <see cref="Document"/>
		/// </summary>
		/// <param name="model"><see cref="AddFilesViewModel"/> object containing Id of the seleted <see cref="Document"/> and uploaded files to be added</param>
		/// <returns>Paths of the added files</returns>
		[HttpPost("addfiles")]
		public JsonResult AddFiles(AddFilesViewModel model)
		{
			if (model == null) return this.ApiBadInputDataResult();
			if (!ModelState.IsValid) return this.ApiBadInputDataResult();
			ICollection<string> result;
			try
			{
				result = _documentRepository.AddFiles(model.DocumentId, model.Files, User.Identity.Name);
			}catch(Exception ex)
			{
				_logger.LogError(new Logger.Log(ex.ToString(), "Update Failed").ToString());
				return this.ApiCustomErrorResult(LocalizationMessageCodes.UpdateError);
			}

			if(result.Count == model.Files.Count)
			{
				_logger.LogInformation(new Logger.Log(result, "Files succesfully added").ToString());
				return this.ApiSuccesResult(new SearchResult { Results = result});
			}
			_logger.LogError(new Logger.Log(model, "Files not added").ToString());
			return this.ApiErrorsResult();
		}
    }
}
