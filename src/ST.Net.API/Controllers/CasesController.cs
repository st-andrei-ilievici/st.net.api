﻿using AgileObjects.AgileMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.DMS;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.Net.API.Data.Case;
using ST.Net.API.Models.Cases;
using ST.Workflow;
using System;
using System.Collections.Generic;

namespace ST.Net.API.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]

	public class CasesController : Controller
    {
		private readonly IDocumentRepository<DataBaseContext> _documentRepository;
		private readonly ILogger _logger;
		public CasesController(IDocumentRepository<DataBaseContext> documentRepository
			,ILoggerFactory loggerFactory)
		{
			_documentRepository = documentRepository;
			_logger = loggerFactory.CreateLogger<CasesController>();
		}

		[HttpPost("save")]
		public JsonResult Save(CreateCaseViewModel model)
		{

			if (model == null)
				return this.ApiBadInputDataResult();
			if (ModelState.IsValid == false)
				return this.ApiBadInputDataResult();
			if (model.DocumentFiles == null)
				return this.ApiBadInputDataResult();

			var tempCase = Mapper.Map(model).ToANew<Case>();
			var document = new Document
			{
				Name = model.DocumentName,
				Description = model.DocumentDescription,
				Keywords = model.DocumentKeywords
			};
			var docResult = _documentRepository.CreateDocument(model.DocumentFiles, document ,  new CaseStrategy());
		
			if (docResult != Guid.Empty)
			{
				tempCase.DocumentId = docResult;
				try
				{
					tempCase.WorkflowId = _documentRepository.GetSingle<Workflow.Workflow>(x => x.ObjectsName == "Case").Id;
					tempCase.StatusId = _documentRepository.GetSingle<Status>(x => x.WorkflowId == tempCase.WorkflowId && x.Name == "Initial").Id;
				}catch(Exception ex)
				{
					_logger.LogError(new Logger.Log(ex.ToString()).ToString());
					return this.ApiCustomErrorResult(LocalizationMessageCodes.SaveError);
				}
				
				var result = _documentRepository.Create(tempCase);
				if(result!= Guid.Empty)
				{
					_logger.LogInformation(new Logger.Log(tempCase, "Entity of type " + typeof(Case) + " saved.").ToString());
					return this.ApiSuccesResult(new SearchResult { Results = new List<Case> { tempCase } });
				}
			}
			_logger.LogError(new Logger.Log(model,"Create Failed").ToString());
			return this.ApiErrorsResult();
		}
	}
}
