﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ST.BaseRepository;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Models;
using ST.Net.API.Core.Security;
using ST.Net.API.Data.Classifier;
using ST.Net.API.Models.Classifier;

namespace ST.Net.API.Controllers
{
    /// <summary>
    /// WebAPI controller of type <see cref="Classifier"/>.
    /// </summary>
    public class ClassifiersController : BaseController
    {
        private readonly IRepository<DataBaseContext> _repository;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor of <see cref="Classifier"/> controller
        /// </summary>
        public ClassifiersController(IRepository<DataBaseContext> repository, ILoggerFactory loggerFactory)
        {
            _repository = repository;
            _logger = loggerFactory.CreateLogger<ClassifiersController>();

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpGet("GetAllClassifierByCode")]
        [SecurityPolicy]
        public IActionResult GetAllClassifierByCode(string code)
        {
            var list = _repository.GetAllIncluding<ClassifierParameter>(p => p.Include(s => s.Classifier))
                .Where(s => s.Classifier.Code == code)
                .Select(s => new ClassifierListViewModel
                {
                    Id = s.Id,
                    Title = s.Title,
                }).ToList();

            var total = list.Count;

            _logger.LogInformation(new Logger.Log(list).ToString());

            foreach (var model in list)
            {
                model.Parameters = _repository.GetAll<ClassifierParameter>()
                    .Where(s => s.CalssifierId == model.Id)
                    .Where(s => s.IsVisible)
                    .Select(s => new ClassifierParameterListViewModel
                    {
                        Id = s.Id,
                        Title = s.Title,
                        Order = s.Order,
                        Comment = s.Comment
                    }).ToList();
            }

            return Json(
                new ApiResultModel
                {
                    IsSucces = true,
                    IsError = false,
                    Result = new SearchResult
                    {
                        Total = total,
                        Results = list
                    }
                }, _serializerSettings);
        }

        [HttpPost("AddClassifier")]
        [SecurityPolicy]
        public IActionResult AddClassifier([FromBody]ClassifierRegistreViewModel model)
        {
            if (ModelState.IsValid == false)
                return View(model);

            var transaction = _repository.BeginTransaction();
            try
            {
                var classifier = new Classifier
                {
                    Title = model.Title,
                    Comment = model.Comment
                };

                var entityId = _repository.Create(classifier);
                if (entityId == classifier.Id)
                {
                    foreach (var param in model.Parameters)
                    {
                        var classifierParameter = new ClassifierParameter
                        {
                            Classifier = classifier,
                            CalssifierId = classifier.Id,
                            Title = param.Title,
                            Comment = param.Comment,
                            Order = param.Order
                        };

                        _repository.Create(classifierParameter);
                    }
                }

                _logger.LogInformation(new Logger.Log("Entity saved.").ToString(), model);

                transaction.Commit();
                return Ok();
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                _logger.LogError(new Logger.Log("Transaction Rollback.").ToString(), model, exception);
            }

            return BadRequest();
        }

        [HttpDelete("DeleteClassifier/{id}")]
        [SecurityPolicy]
        public IActionResult Delete(Guid id)
        {
            if (id == Guid.Empty)
                return BadRequest();

            var transaction = _repository.BeginTransaction();
            try
            {
                var classifier = _repository.GetSingle<Classifier>(s => s.Id == id);
                if (classifier == null)
                {
                    _logger.LogWarning(new Logger.Log("Classifier Entity not found.").ToString(), id);
                    return BadRequest();
                }

                classifier.IsDeleted = true;
                classifier.ModifiedAt = DateTime.Now;

                _repository.Update(classifier);

                var parameters = _repository.GetAll<ClassifierParameter>()
                    .Where(s => s.CalssifierId == classifier.Id)
                    .ToList();

                foreach (var classifierParameter in parameters)
                {
                    classifierParameter.IsDeleted = classifier.IsDeleted;
                    classifierParameter.ModifiedAt = classifier.ModifiedAt;

                    _repository.Update(classifierParameter);
                }

                if (parameters.Any())
                    _logger.LogError(new Logger.Log("Classifier Parameters has been deleted.").ToString(), parameters);

                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Accepted;

                transaction.Commit();
                return Ok();
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                _logger.LogError(new Logger.Log("Transaction Rollback.").ToString(), id, exception);
            }

            return BadRequest();
        }

        [HttpPut("UpdateClassifier/{id}")]
        [SecurityPolicy]
        public IActionResult Update(Guid id, [FromBody] ClassifierUpdateViewModel model)
        {
            if (ModelState.IsValid == false)
                return View(model);

            var transaction = _repository.BeginTransaction();
            try
            {
                var classifier = _repository.GetSingle<Classifier>(s => s.Id == id);
                if (classifier == null)
                {
                    _logger.LogWarning(new Logger.Log("Classifier Entity not found.").ToString(), id);
                    return BadRequest();
                }

                classifier.IsDeleted = model.IsDeleted;
                classifier.IsVisible = model.IsVisible;
                classifier.Comment = model.Comment;
                classifier.Title = model.Title;
                classifier.ModifiedAt = DateTime.Now;

                _repository.Update(classifier);

                var parameters = _repository.GetAll<ClassifierParameter>()
                    .Where(s => s.CalssifierId == classifier.Id)
                    .ToList();

                foreach (var classifierParameter in parameters)
                {
                    classifierParameter.IsDeleted = classifier.IsDeleted;
                    classifierParameter.ModifiedAt = classifier.ModifiedAt;

                    _repository.Update(classifierParameter);
                }

                if (parameters.Any())
                    _logger.LogError(new Logger.Log("Classifier Parameters has been updated.").ToString(), parameters);

                HttpContext.Response.ContentType = "application/json";
                HttpContext.Response.StatusCode = (int)HttpStatusCode.Accepted;

                transaction.Commit();
                return Ok();
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                _logger.LogError(new Logger.Log("Transaction Rollback.").ToString(), id, exception);
            }

            return BadRequest();
        }
    }
}
