using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.Models;
using ST.Net.API.Core.Security;
using ST.Net.API.Data.Court;
using ST.Net.API.Models.Region;

namespace ST.Net.API.Controllers
{
    /// <summary>
    /// WebAPI controller of type <see cref="Region"/>.
    /// </summary>
    public class RegionsController : BaseController
    {
        private readonly IReadController<RegionsController, Region, RegionListViewModel> _readable;
        
        /// <summary>
        /// Constructor of <see cref="Region"/> controller
        /// </summary>
        public RegionsController(IReadController<RegionsController, Region, RegionListViewModel> readable)
        {
            _readable = readable;
        }

        /// <summary>
        /// Obtain regions
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>
        [HttpGet("getAll")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(IList<SuccesResultModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public IActionResult Get()
        {
            return _readable.Get();
        }
    }
}