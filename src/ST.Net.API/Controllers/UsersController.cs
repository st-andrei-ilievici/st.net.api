using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using ST.Net.API.Models.Users;
using ST.UserIdentity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.Net.API.Core.Security;

namespace ST.Net.API.Controllers
{
    /// <summary>
    /// User Web API controller.
    /// </summary>
    public class UsersController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor of <see cref="User"/> controller
        /// </summary>
        public UsersController(
            ILoggerFactory loggerFactory,
            UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _logger = loggerFactory.CreateLogger<UsersController>();

            _userManager = userManager;
            _roleManager = roleManager;
        }

        /// <summary>
        /// Registers a new user.
        /// </summary>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>    
        [AllowAnonymous]
        [HttpPost("create")]
        [ProducesResponseType(typeof(IList<SuccesResultModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public async Task<JsonResult> Create([FromBody] RegisterViewModel model)
        {
            if (model == null)
                return this.ReturnBadRequest();

            var entity = new User
            {
                Id = Guid.NewGuid().ToString("N"),
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                BirthDate = model.BirthDate,
                UserName = model.Email,
                AddedAt = DateTime.Now,
                PhoneNumber = model.PhoneNumber,
                FullName = string.Format("{0} {1}", model.FirstName, model.LastName)
            };

            entity.UserName = entity.Email;
            entity.AddedAt = DateTime.Now;
            entity.PasswordHash = new PasswordHasher<User>().HashPassword(entity, model.Password);

            //Role
            var role = new IdentityRole { Name = "User" };
            if (await _roleManager.RoleExistsAsync(role.Name) == false)
                await _roleManager.CreateAsync(role);

            var createResult = await _userManager.CreateAsync(entity, model.Password);
            if (createResult.Succeeded == false)
                return this.ReturnBadRequest(createResult);

            var identityResult = await _userManager.AddToRoleAsync(entity, role.Name);
            if (identityResult.Succeeded)
                _logger.LogInformation("Default role has beed atached", entity, role);

            _logger.LogInformation(new Logger.Log(entity).ToString());

            var searchResult = new SearchResult
            {
                Results = new List<RegisterViewModel> { model }
            };

            return this.ReturnSearchResult(searchResult);
        }

        /// <summary>
        /// Obtain users by page
        /// </summary>
        /// <param name="page">Page</param>
        /// <param name="pageSize">Total page size</param>
        /// <remarks>
        /// Note that the PAGE value must be greater than zero.
        /// </remarks>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>
        [HttpGet("getPage")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(IList<SuccesResultModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public JsonResult GetByPage(int page, int pageSize = 10)
        {
            if (page <= 0)
                return this.ReturnBadRequest(LocalizationMessageCodes.PageLessThanZero);

            var list = _userManager.Users
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .Select(s => new IndexUserViewModel
                {
                    Id = s.Id,
                    Email = s.Email,
                    FullName = s.FullName
                })
                .ToList();

            var total = list.Count;

            _logger.LogInformation(new Logger.Log(list).ToString());

            var searchResult = new SearchResult
            {
                Total = total,
                Page = page,
                PageSize = pageSize,
                Results = list
            };

            return this.ReturnSearchResult(searchResult);
        }

        /// <summary>
        /// Delete user entity
        /// </summary>
        /// <param name="id">Entity ID</param>
        /// <remarks>
        /// Note that the ID parameter is a GUID and not a string.
        /// </remarks>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">User not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>
        [HttpDelete("delete/{id}")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var strId = id.ToString("N");
            var user = await _userManager.Users.FirstOrDefaultAsync(s => s.Id == strId);
            if (user == null)
                return this.ReturnEntityNotFound();

            var deleteResult = await _userManager.DeleteAsync(user);
            if (deleteResult.Succeeded == false)
                return this.ReturnBadRequest(deleteResult);

            var searchResult = new SearchResult
            {
                Results = new List<IndexUserViewModel>
                {
                    new IndexUserViewModel
                    {
                        Id = user.Id,
                        Email = user.Email,
                        FullName = user.FullName
                    }
                }
            };

            return this.ApiSuccesResult(searchResult);
        }

        /// <summary>
        /// Update user entity
        /// </summary>
        /// <remarks>
        /// Note that the ID parameter is a GUID and not a string.
        /// </remarks>
        /// <param name="id" type="Guid">Uniqueidentifier of the entity</param>
        /// <param name="model">Model</param>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="422">Unprocessable Entity (validation error)</response>
        [HttpPut("update/{id}")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)CustomHttpStatusCode.UnprocessableEntity)]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateUserViewModel model)
        {
            var strId = id.ToString("N");

            var userExists = await _userManager.Users.FirstOrDefaultAsync(s => s.Id == strId);
            if (userExists == null)
                throw new Exception("User not found");

            if (userExists.UserName != model.UserName)
            {
                // username has changed so check if the new username is already taken
                if (_userManager.FindByNameAsync(model.UserName) != null)
                    throw new Exception(nameof(model.UserName) + " " + model.UserName + " is already taken");
            }

            // update user properties
            userExists.FirstName = model.FirstName;
            userExists.LastName = model.LastName;
            userExists.UserName = model.UserName;
            userExists.PasswordHash = new PasswordHasher<User>().HashPassword(userExists, model.Password);

            return this.ApiSuccesResult();
        }
    }
}