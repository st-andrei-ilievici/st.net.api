﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.UserIdentity;
using Microsoft.Extensions.Logging;

namespace ST.Net.API.Controllers
{
    /// <summary>
    /// WebAPI controller of type <see cref="Permission"/>.
    /// </summary>
    public class PermissionsController : BaseController
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IPermissionRepository<DataBaseContext> _permissionRepository;
        private readonly JsonSerializerSettings _serializerSettings;
        private readonly ILogger _logger;
        
        /// <summary>
        /// Constructor of <see cref="Permission"/> controller
        /// </summary>
        public PermissionsController(
            ILoggerFactory loggerFactory,
            IPermissionRepository<DataBaseContext>
            permissionRepository, UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _logger = loggerFactory.CreateLogger<PermissionsController>();

            _permissionRepository = permissionRepository;
            _userManager = userManager;
            _roleManager = roleManager;

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Return permission to given entity by prefix
        /// </summary>
        [HttpGet("GetByCategory")]
        public async Task<IActionResult> GetByCategory(string category, string prefix = null)
        {
            var items = _permissionRepository
                .GetAll<Permission>();

            if (string.IsNullOrEmpty(prefix))
                items = items
                    .Where(e => e.Category == category);
            else
                items = items
                    .Where(e => e.Category == category)
                    .Where(e => e.Name.Contains(prefix));

            items = items.ToList();

            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                _logger.LogWarning("GetPermissionList by category failed: Unauthorized", category);
                return this.ApiCustomErrorResult(LocalizationMessageCodes.Unauthorized);
            }

            var roles = _roleManager.Roles.ToList();

            var checkedPermissions = new Dictionary<string, bool>();

            foreach (var item in roles)
            {
                if (await _userManager.IsInRoleAsync(user, item.Name))
                {
                    foreach (var perm in items)
                    {
                        checkedPermissions.Add(perm.Name, _permissionRepository.PermissionHasRole(perm, item.Name));
                    }
                }
            }

            var model = new ApiResultModel
            {
                IsSucces = true,
                IsError = false,
                Result = checkedPermissions
            };

            return Json(model, _serializerSettings);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<User> GetCurrentUserAsync()
        {
            var userEmail = this.GetAuthorizedUserEmail();

            return await _userManager.FindByEmailAsync(userEmail);
        }
    }
}
