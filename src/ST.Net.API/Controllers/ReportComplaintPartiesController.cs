﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Models.ReportComplaintParty;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.Net.API.Core.Security;
using ST.Net.API.Data.Complaint;

namespace ST.Net.API.Controllers
{
    /// <summary>
    /// WebAPI controller of type <see cref="ReportComplaintParty"/>.
    /// </summary>
    public class ReportComplaintPartiesController : BaseController
    {
        private readonly IUpdateController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyUpdateViewModel> _updateable;
        private readonly IDeleteController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyListViewModel> _deletable;
        private readonly IReadController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyListViewModel> _getPageController;
        private readonly ICreateController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyRegistreViewModel> _createable;

        /// <summary>
        /// Constructor of <see cref="ReportComplaintParty"/> controller
        /// </summary>
        public ReportComplaintPartiesController(
            IUpdateController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyUpdateViewModel> updateable,
            IDeleteController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyListViewModel> deletable,
            IReadController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyListViewModel> getPageController,
            ICreateController<ReportComplaintPartiesController, ReportComplaintParty, ReportComplaintPartyRegistreViewModel> createable)
        {
            _updateable = updateable;
            _deletable = deletable;
            _getPageController = getPageController;
            _createable = createable;
        }

        /// <summary>
        /// Update ReportComplaint Party entity
        /// </summary>
        /// <remarks>
        /// Note that the ID parameter is a GUID and not a string.
        /// </remarks>
        /// <param name="id" type="Guid">Uniqueidentifier of the entity</param>
        /// <param name="model">Model</param>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="422">Unprocessable Entity (validation error)</response>
        [HttpPut("update/{id}")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)CustomHttpStatusCode.UnprocessableEntity)]
        public IActionResult Update(Guid id, [FromBody] ReportComplaintPartyUpdateViewModel model)
        {
            return _updateable.Update(id, model);
        }

        /// <summary>
        /// Registers a ReportComplaint Party entity.
        /// </summary>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>    
        [HttpPost("create")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(SuccesResultModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public IActionResult Create([FromBody] ReportComplaintPartyRegistreViewModel model)
        {
            return _createable.Create(model);
        }

        /// <summary>
        /// Obtain ReportComplaint parties by page
        /// </summary>
        /// <param name="page">Page</param>
        /// <param name="pageSize">Total page size</param>
        /// <remarks>
        /// Note that the PAGE value must be greater than zero.
        /// </remarks>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">Member not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>
        [HttpGet("getPage")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(SuccesResultModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public IActionResult GetByPage(int page, int pageSize = 10)
        {
            Func<IQueryable<ReportComplaintParty>, IQueryable<ReportComplaintParty>> includeDelegate =
                p => p.Include(f => f.Person)
                    .Include(f => f.ReportComplaint);

            return _getPageController.GetByPageInclude(page, includeDelegate, pageSize);
        }

        /// <summary>
        /// Delete ReportComplaint party entity
        /// </summary>
        /// <param name="id">Entity ID</param>
        /// <remarks>
        /// Note that the ID parameter is a GUID and not a string.
        /// </remarks>
        /// <response code="200">Successful request</response>
        /// <response code="400">Bad request</response>
        /// <response code="401">Token expired</response>
        /// <response code="404">User not found</response>
        /// <response code="405">Method is not allowed</response>
        /// <response code="500">Internal error</response>
        [HttpDelete("delete/{id}")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.NotFound)]
        public IActionResult Delete(Guid id)
        {
            return _deletable.Delete(id);
        }

        /// <summary>
        /// Retrieves a specific complaint party by unique id
        /// </summary>
        /// <response code="200">Successful request</response>
        /// <response code="400">Complaint party has missing/invalid values</response>
        /// <response code="500">Oops! Can't create your complaint party right now</response>
        [HttpGet("getById/{id}")]
        [SecurityPolicy]
        [ProducesResponseType(typeof(SuccesResultModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.Unauthorized)]
        [ProducesResponseType(typeof(ExceptionResultModel), (int)HttpStatusCode.MethodNotAllowed)]
        public IActionResult GetById(Guid id)
        {
            Func<IQueryable<ReportComplaintParty>, IQueryable<ReportComplaintParty>> includeDelegate =
                p => p.Include(f => f.Person)
                    .Include(f => f.ReportComplaint);

            return _getPageController.GetByIdInclude(id, includeDelegate);
        }
    }
}
