﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ST.Net.API.Core.EntityFramework;
using ST.Notifications;

namespace ST.Net.API.Migrations
{
    [DbContext(typeof(DataBaseContext))]
    [Migration("20170804141906_CasesAndDocuments")]
    partial class CasesAndDocuments
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("ST.DMS.Document", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Description");

                    b.Property<string>("DirectoryPath");

                    b.Property<Guid>("FolderId");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Keywords");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("FolderId");

                    b.ToTable("Documents");
                });

            modelBuilder.Entity("ST.DMS.DocumentConfiguration", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("ServerPath");

                    b.HasKey("Id");

                    b.ToTable("DocumentConfigurations");
                });

            modelBuilder.Entity("ST.DMS.FileVersion", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<Guid>("DocumentId");

                    b.Property<string>("FileExtension");

                    b.Property<string>("FileName");

                    b.Property<Guid?>("FolderId");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Path");

                    b.Property<float>("Version");

                    b.HasKey("Id");

                    b.HasIndex("DocumentId");

                    b.HasIndex("FolderId");

                    b.ToTable("FileVersions");
                });

            modelBuilder.Entity("ST.DMS.Folder", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<Guid?>("ParentFolderId");

                    b.HasKey("Id");

                    b.HasIndex("ParentFolderId");

                    b.ToTable("Folders");
                });

            modelBuilder.Entity("ST.Net.API.Data.Case.Case", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<Guid>("ClassifierId");

                    b.Property<Guid>("DocumentId");

                    b.Property<DateTime>("EndDate");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsPublic");

                    b.Property<int>("MaxYears");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<DateTime>("StartDate");

                    b.Property<Guid>("StatusId");

                    b.Property<Guid>("WorkflowId");

                    b.HasKey("Id");

                    b.HasIndex("ClassifierId");

                    b.HasIndex("DocumentId");

                    b.HasIndex("StatusId");

                    b.HasIndex("WorkflowId");

                    b.ToTable("Cases");
                });

            modelBuilder.Entity("ST.Net.API.Data.Classifier.Classifier", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Code");

                    b.Property<string>("Comment");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsVisible");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Classifiers","classifier");
                });

            modelBuilder.Entity("ST.Net.API.Data.Classifier.ClassifierParameter", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<Guid?>("ClassifierId");

                    b.Property<string>("Comment");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsVisible");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<int>("Order");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("ClassifierId");

                    b.ToTable("ClassifierParameters","classifier");
                });

            modelBuilder.Entity("ST.Net.API.Data.complaint.ReportComplaint", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ActionValue");

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<int>("Annex");

                    b.Property<DateTime>("ArchivedOn");

                    b.Property<Guid>("AutomaticCode");

                    b.Property<string>("CancellationAuthor");

                    b.Property<string>("CancellationComment");

                    b.Property<DateTime>("CancellationDate");

                    b.Property<Guid>("CancellationParticipantId");

                    b.Property<bool>("CaseCategoryChecked");

                    b.Property<Guid>("CaseCateogryId");

                    b.Property<Guid>("CaseKindId");

                    b.Property<Guid>("CaseLinkId");

                    b.Property<string>("Comments");

                    b.Property<DateTime>("CourtDateTo");

                    b.Property<Guid>("CourtId");

                    b.Property<DateTime>("Created");

                    b.Property<string>("CurentState");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsResolved");

                    b.Property<bool>("IsSameJudge");

                    b.Property<bool>("IsSecret");

                    b.Property<Guid>("Judge_Id");

                    b.Property<string>("LinkReportComplaint");

                    b.Property<Guid>("Main_ReportComplaintId");

                    b.Property<string>("ManualCode");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Nature");

                    b.Property<bool>("NotBeAssesed");

                    b.Property<Guid>("OldCaseLinkId");

                    b.Property<bool>("ReportIsActive");

                    b.Property<Guid>("RequestFormId");

                    b.Property<string>("SendAuthor");

                    b.Property<string>("SendComments");

                    b.Property<string>("SendCourt");

                    b.Property<DateTime>("SendDate");

                    b.Property<float>("StateFee");

                    b.Property<Guid>("StateFeeReasonId");

                    b.Property<Guid>("StatusId");

                    b.Property<bool>("SubTypeChecked");

                    b.Property<Guid>("SubTypeId");

                    b.Property<string>("Title");

                    b.Property<DateTime>("TransferedDate");

                    b.Property<Guid>("TypeExamId");

                    b.Property<int>("Volumes");

                    b.Property<Guid>("WorkflowId");

                    b.HasKey("Id");

                    b.HasIndex("CourtId");

                    b.HasIndex("StatusId");

                    b.HasIndex("WorkflowId");

                    b.ToTable("ReportComplaints","complaint");
                });

            modelBuilder.Entity("ST.Net.API.Data.complaint.ReportComplaintParty", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("GetFreeJudAssistance");

                    b.Property<bool>("IsActive");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("PartyComments");

                    b.Property<DateTime?>("PartyDateAdd");

                    b.Property<DateTime?>("PartyDateEnd");

                    b.Property<Guid>("PersonId");

                    b.Property<Guid>("ReportComplaintId");

                    b.HasKey("Id");

                    b.HasIndex("PersonId");

                    b.HasIndex("ReportComplaintId");

                    b.ToTable("ReportComplaintParties","complaint");
                });

            modelBuilder.Entity("ST.Net.API.Data.court.Court", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Appartment");

                    b.Property<string>("Block");

                    b.Property<string>("Code");

                    b.Property<string>("Email");

                    b.Property<string>("Fax");

                    b.Property<string>("Home");

                    b.Property<string>("Idno");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Location");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Phone");

                    b.Property<Guid>("RegionId");

                    b.Property<string>("Street");

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.HasIndex("RegionId");

                    b.ToTable("Court","court");
                });

            modelBuilder.Entity("ST.Net.API.Data.court.Person", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<Guid>("CourtId");

                    b.Property<string>("FirstName");

                    b.Property<string>("Function");

                    b.Property<string>("Gender");

                    b.Property<string>("Idnp");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("LastName");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Surname");

                    b.HasKey("Id");

                    b.HasIndex("CourtId");

                    b.ToTable("Persons","court");
                });

            modelBuilder.Entity("ST.Net.API.Data.court.Region", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Regions","court");
                });

            modelBuilder.Entity("ST.Notifications.Notification", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("Message");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<string>("Subject");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.ToTable("Notifications");
                });

            modelBuilder.Entity("ST.UserIdentity.Permission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Category");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Permissions");
                });

            modelBuilder.Entity("ST.UserIdentity.RolePermission", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<Guid>("PermissionId");

                    b.Property<string>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("PermissionId");

                    b.HasIndex("RoleId");

                    b.ToTable("RolePermissions");
                });

            modelBuilder.Entity("ST.UserIdentity.User", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<DateTime?>("BirthDate");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("FullName");

                    b.Property<int?>("Gender");

                    b.Property<bool>("IsDisabled");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("Uniquecode");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("ST.UserIdentity.UserSession", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("ForceLogout");

                    b.Property<string>("Ip");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime>("LoginAt");

                    b.Property<DateTime?>("LogoutAt");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Referer");

                    b.Property<string>("SessionId");

                    b.Property<string>("Uniquecode");

                    b.Property<string>("UserAgent");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserSessions");
                });

            modelBuilder.Entity("ST.Vocabulary.Vocabulary", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<string>("Words");

                    b.HasKey("Id");

                    b.ToTable("Vocabularies");
                });

            modelBuilder.Entity("ST.Workflow.Action", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Description");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<Guid?>("NotificationId");

                    b.HasKey("Id");

                    b.HasIndex("NotificationId");

                    b.ToTable("Actions");
                });

            modelBuilder.Entity("ST.Workflow.Status", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<long?>("Deadline");

                    b.Property<string>("Description");

                    b.Property<bool>("IsDeleted");

                    b.Property<bool>("IsFinal");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<Guid>("WorkflowId");

                    b.HasKey("Id");

                    b.HasIndex("WorkflowId");

                    b.ToTable("Statuses");
                });

            modelBuilder.Entity("ST.Workflow.StatusFields", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("RoleId");

                    b.Property<string>("Settings");

                    b.Property<Guid>("StatusId");

                    b.Property<Guid>("WorkflowId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.HasIndex("StatusId");

                    b.HasIndex("WorkflowId");

                    b.ToTable("StatusFields");
                });

            modelBuilder.Entity("ST.Workflow.Transition", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("ActionId");

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<Guid>("EntryStatusId");

                    b.Property<Guid>("ExitStatusId");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("RoleId");

                    b.Property<Guid>("WorkflowId");

                    b.HasKey("Id");

                    b.HasIndex("ActionId");

                    b.HasIndex("EntryStatusId");

                    b.HasIndex("ExitStatusId");

                    b.HasIndex("RoleId");

                    b.HasIndex("WorkflowId");

                    b.ToTable("Transitions");
                });

            modelBuilder.Entity("ST.Workflow.Workflow", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("AddedAt");

                    b.Property<string>("AddedById");

                    b.Property<string>("Description");

                    b.Property<bool>("IsDeleted");

                    b.Property<DateTime?>("ModifiedAt");

                    b.Property<string>("ModifiedById");

                    b.Property<string>("Name");

                    b.Property<string>("ObjectsName");

                    b.HasKey("Id");

                    b.ToTable("Workflows");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("ST.UserIdentity.User")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("ST.UserIdentity.User")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.UserIdentity.User")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.DMS.Document", b =>
                {
                    b.HasOne("ST.DMS.Folder", "Folder")
                        .WithMany()
                        .HasForeignKey("FolderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.DMS.FileVersion", b =>
                {
                    b.HasOne("ST.DMS.Document", "Document")
                        .WithMany("FileVersions")
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.DMS.Folder", "Folder")
                        .WithMany()
                        .HasForeignKey("FolderId");
                });

            modelBuilder.Entity("ST.DMS.Folder", b =>
                {
                    b.HasOne("ST.DMS.Folder", "ParentFolder")
                        .WithMany()
                        .HasForeignKey("ParentFolderId");
                });

            modelBuilder.Entity("ST.Net.API.Data.Case.Case", b =>
                {
                    b.HasOne("ST.Net.API.Data.Classifier.Classifier", "Classifier")
                        .WithMany()
                        .HasForeignKey("ClassifierId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.DMS.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Workflow.Status", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Workflow.Workflow", "Workflow")
                        .WithMany()
                        .HasForeignKey("WorkflowId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.Net.API.Data.Classifier.ClassifierParameter", b =>
                {
                    b.HasOne("ST.Net.API.Data.Classifier.Classifier", "Classifier")
                        .WithMany()
                        .HasForeignKey("ClassifierId");
                });

            modelBuilder.Entity("ST.Net.API.Data.complaint.ReportComplaint", b =>
                {
                    b.HasOne("ST.Net.API.Data.court.Court", "Court")
                        .WithMany()
                        .HasForeignKey("CourtId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Workflow.Status", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Workflow.Workflow", "Workflow")
                        .WithMany()
                        .HasForeignKey("WorkflowId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.Net.API.Data.complaint.ReportComplaintParty", b =>
                {
                    b.HasOne("ST.Net.API.Data.court.Person", "Person")
                        .WithMany()
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Net.API.Data.complaint.ReportComplaint", "ReportComplaint")
                        .WithMany()
                        .HasForeignKey("ReportComplaintId");
                });

            modelBuilder.Entity("ST.Net.API.Data.court.Court", b =>
                {
                    b.HasOne("ST.Net.API.Data.court.Region", "Region")
                        .WithMany()
                        .HasForeignKey("RegionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.Net.API.Data.court.Person", b =>
                {
                    b.HasOne("ST.Net.API.Data.court.Court", "Court")
                        .WithMany()
                        .HasForeignKey("CourtId");
                });

            modelBuilder.Entity("ST.UserIdentity.Permission", b =>
                {
                    b.HasOne("ST.UserIdentity.User")
                        .WithMany("Permissions")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("ST.UserIdentity.RolePermission", b =>
                {
                    b.HasOne("ST.UserIdentity.Permission", "Permission")
                        .WithMany("RolePermissions")
                        .HasForeignKey("PermissionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("ST.UserIdentity.UserSession", b =>
                {
                    b.HasOne("ST.UserIdentity.User", "User")
                        .WithMany("UserSessions")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("ST.Workflow.Action", b =>
                {
                    b.HasOne("ST.Notifications.Notification", "Notification")
                        .WithMany()
                        .HasForeignKey("NotificationId");
                });

            modelBuilder.Entity("ST.Workflow.Status", b =>
                {
                    b.HasOne("ST.Workflow.Workflow", "Workflow")
                        .WithMany("Statuses")
                        .HasForeignKey("WorkflowId");
                });

            modelBuilder.Entity("ST.Workflow.StatusFields", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("ST.Workflow.Status", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("ST.Workflow.Workflow", "Workflow")
                        .WithMany("StatusFields")
                        .HasForeignKey("WorkflowId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ST.Workflow.Transition", b =>
                {
                    b.HasOne("ST.Workflow.Action", "Action")
                        .WithMany()
                        .HasForeignKey("ActionId");

                    b.HasOne("ST.Workflow.Status", "EntryStatus")
                        .WithMany()
                        .HasForeignKey("EntryStatusId");

                    b.HasOne("ST.Workflow.Status", "ExitStatus")
                        .WithMany()
                        .HasForeignKey("ExitStatusId");

                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", "Role")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("ST.Workflow.Workflow", "Workflow")
                        .WithMany("Transitions")
                        .HasForeignKey("WorkflowId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
