﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Filters;
using ST.Net.API.Core.Middlewares;
using ST.Net.API.Extensions;
using ST.UserIdentity;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using AgileObjects.AgileMapper;

namespace ST.Net.API
{
    public partial class Startup
    {
        public IConfigurationRoot Configuration { get; set; }

        /// <summary>
        /// WebAPI Startup method
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile("oauth_server.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(opts =>
            {
                opts.AddPolicy("CorsGlobal", policy =>
                {
                    policy.AllowAnyHeader();
                    policy.AllowAnyOrigin();
                    policy.AllowAnyMethod();
                });
            });

            // Database Context
            services.AddDbContext<DataBaseContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("api.db.context"),
                    b => b.MigrationsAssembly("ST.Net.API"));
            });

            // Identity
            services.AddIdentity<User, IdentityRole>(options =>
                {
                    options.Cookies.ApplicationCookie.Events = new CookieAuthenticationEvents
                    {
                        OnRedirectToLogin = ctx =>
                        {
                            ctx.Response.ContentType = "application/json";
                            ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                            //if (ctx.Request.Path.StartsWithSegments("/api") && ctx.Response.StatusCode == (int)HttpStatusCode.OK)
                            //{
                            //    ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                            //}
                            //else
                            //{
                            //    ctx.Response.Redirect(ctx.RedirectUri);
                            //}
                            return Task.FromResult(0);
                        }
                    };
                })
                .AddEntityFrameworkStores<DataBaseContext>()
                .AddDefaultTokenProviders();

            // ReferenceLooping in JSON Serialization
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(ModelValidationFilter));
                })
                .AddJsonOptions(options =>
                {
                    // handle loops correctly
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

                    // use standard name conversion of properties
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("CorsGlobal"));
            });

            //WebAPI
            services.RegistreApiServices();
            services.RegistreApiRepositories();
            services.RegistreApiSwagger(Configuration);

            //SecurityPolicy
            services.RegistreSecurityPolicy();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //Is Development
            app.AddDevelopmentMiddlewares(env, Configuration);

            //add this line to show the ERROR STATUS CODES
            app.UseStatusCodePages();

            //Enable CORS before calling app.UseMvc() and app.UseStaticFiles()
            app.UseCors("CorsGlobal");

            // Microsoft.AspNetCore.StaticFiles: API for starting the application from wwwroot.
            // Uses default files as index.html.
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseIdentity();

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
            {
                Authority = Configuration["IdentityServer:Authority"],
                ApiName = Configuration["IdentityServer:ApiName"],
                RequireHttpsMetadata = false
            });

            //Middleware
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));

            ConfigureSwagger(app);

            //Enabled auth through bearer tokens
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
