﻿using Microsoft.AspNetCore.Builder;

namespace ST.Net.API
{
    public partial class Startup
    {
        private void ConfigureSwagger(IApplicationBuilder app)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/doc/swagger.json", "PIGD WebAPI");

                //c.EnabledValidator();
                c.BooleanValues(new object[] { 0, 1 });
                c.DocExpansion("none");
                c.SupportedSubmitMethods(new[] { "get", "post", "put", "patch" });

                c.ConfigureOAuth2("PIGD Swagger", null, null, "Swagger");
            });
        }
    }
}
