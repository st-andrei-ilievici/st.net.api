﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using ST.Net.API.Core.EntityFramework;

namespace ST.Net.API.AuthServer.Configurations
{
    /// <summary>
    /// Custom profile for OAuth Server
    /// </summary>
    internal class CustomProfileService : IProfileService
    {
        private readonly DataBaseContext _context;

        public CustomProfileService(DataBaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// This method is called whenever claims about the user are requested (e.g. duringtoken creation or via the userinfo endpoint)
        /// </summary>
        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            try
            {
                var subjectId = context.Subject.GetSubjectId();

                var user = _context.Users.FirstOrDefault(u => u.Id == subjectId);
                if (user == null)
                    return Task.FromResult(0);

                var claims = new List<Claim>
                {
                    new Claim(JwtClaimTypes.Subject, user.Id)
                };
                
                if (!string.IsNullOrEmpty(user.UserName))
                    claims.Add(new Claim(JwtClaimTypes.Name, user.UserName));

                if (!string.IsNullOrEmpty(user.FirstName))
                    claims.Add(new Claim(JwtClaimTypes.Name, user.FirstName));

                if (!string.IsNullOrEmpty(user.LastName))
                    claims.Add(new Claim(JwtClaimTypes.Name, user.LastName));

                if (!string.IsNullOrEmpty(user.Email))
                    claims.Add(new Claim(JwtClaimTypes.Name, user.Email));

                if (!string.IsNullOrEmpty(user.Email))
                {
                    claims.Add(new Claim(JwtClaimTypes.Name, user.Email));
                    claims.Add(new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean));
                }

                context.IssuedClaims = claims;
                return Task.FromResult(0);
            }
            catch (Exception)
            {
                return Task.FromResult(0);
            }
        }

        /// <summary>
        ///  This method gets called whenever identity server needs to determine if the user is valid or active (e.g. if the user's account has been deactivated since they logged in). (e.g. during token issuance or validation).
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task IsActiveAsync(IsActiveContext context)
        {
            var subjectId = context.Subject.GetSubjectId();

            var user = _context.Users.FirstOrDefault(u => u.Id == subjectId);
            if (user == null)
                return Task.FromResult(0);

            //if(user.IsDisabled)

            context.IsActive = true;

            return Task.FromResult(0);
        }
    }
}
