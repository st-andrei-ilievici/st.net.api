﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.AuthServer.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
