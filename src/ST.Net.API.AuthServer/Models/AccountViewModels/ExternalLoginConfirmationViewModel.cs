﻿using System.ComponentModel.DataAnnotations;

namespace ST.Net.API.AuthServer.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
