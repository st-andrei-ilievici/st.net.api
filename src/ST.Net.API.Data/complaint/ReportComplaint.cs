﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.Workflow;

namespace ST.Net.API.Data.Complaint
{
	public class ReportComplaint : STObject
	{
		public string Title { get; set; }
		public Guid CaseLinkId { get; set; }
		public Guid CaseKindId { get; set; }
		public Guid AutomaticCode { get; set; }
		public string ManualCode { get; set; }
		public Guid TypeExamId { get; set; }
		public Guid SubTypeId { get; set; }
		public bool IsSecret { get; set; }
		public string LinkReportComplaint { get; set; }
		public bool IsSameJudge { get; set; }
		public Guid RequestFormId { get; set; }
		public Guid CaseCateogryId { get; set; }
		public string Nature { get; set; }
		public int Volumes { get; set; }
		public int Annex { get; set; }
		public float StateFee { get; set; }
		public Guid StateFeeReasonId { get; set; }
		public string Comments { get; set; }
		public Guid CourtId { get; set; }
		public string CurentState { get; set; }
		public DateTime Created { get; set; }
		public Guid OldCaseLinkId { get; set; }
		public bool IsResolved { get; set; }
		public DateTime CancellationDate { get; set; }
		public string CancellationComment { get; set; }
		public string CancellationAuthor { get; set; }
		public Guid CancellationParticipantId { get; set; }
		public string SendCourt { get; set; }
		public DateTime SendDate { get; set; }
		public string SendAuthor { get; set; }
		public string SendComments { get; set; }
		public bool ReportIsActive { get; set; }
		public DateTime CourtDateTo { get; set; }
		public DateTime TransferedDate { get; set; }
		public DateTime ArchivedOn { get; set; }
		public Guid Judge_Id { get; set; }
		public Guid Main_ReportComplaintId { get; set; }
		public bool NotBeAssesed { get; set; }
		public int ActionValue { get; set; }
		public bool SubTypeChecked { get; set; }
		public bool CaseCategoryChecked { get; set; }


        public Court.Court Court { get; set; }

        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<ReportComplaint>();
            model.ToTable("ReportComplaints", "complaint");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}
