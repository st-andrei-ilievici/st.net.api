﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;
using ST.Net.API.Data.Court;

namespace ST.Net.API.Data.Complaint
{
    public class ReportComplaintParty : BaseModel
    {
        public new Guid Id { get; set; }

        public ReportComplaint ReportComplaint { get; set; }
        public Guid ReportComplaintId { get; set; }


        public Person Person { get; set; }
        public Guid PersonId { get; set; }

        //public User User { get; set; }
        //public Guid UserId { get; set; }



        //public ReportComplaintPartyType ReportComplaintPartyType { get; set; }
        //public Guid ReportComplaintPartyTypeId { get; set; }



        public string PartyComments { get; set; }

        public DateTime? PartyDateAdd { get; set; }

        public DateTime? PartyDateEnd { get; set; }

        public bool GetFreeJudAssistance { get; set; }

        public bool IsActive { get; set; }




        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<ReportComplaintParty>();
            model.ToTable("ReportComplaintParties", "complaint");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}