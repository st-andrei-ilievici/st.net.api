﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;

namespace ST.Net.API.Data.Court
{
    public class Person : BaseModel
    {
        public new Guid Id { get; set; }

        public Court Court { get; set; }

        public Guid CourtId { get; set; }


        //public User User { get; set; }

        //public Guid PersonRegionID { get; set; }

        //public Guid PersonEducationID { get; set; }

        //public Guid PersonOccupationID { get; set; }

        //public bool PersonPleaBargain { get; set; }

        //public bool PersonMinor { get; set; }

        //public bool Audiati { get; set; }

        //public Guid PersonCitizenshipID { get; set; }

        //public Guid PersonCatID { get; set; }


        //public Guid MinorType { get; set; }

        //public Guid motivnescolarizare { get; set; }

        //public string Region { get; set; }

        //public string TypeActivity { get; set; }

        public string Function { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Idnp { get; set; }

        public string Gender { get; set; }

        //public string Type { get; set; }

        //public string Location { get; set; }

        //public string Street { get; set; }

        //public string Home { get; set; }

        //public string Appartment { get; set; }

        //public string Phone { get; set; }

        //public string CellPhone { get; set; }

        //public string Email { get; set; }

        //public string ContactInfo { get; set; }

        //public DateTime? PersonDateOfBirth { get; set; }

        //public DateTime? PersonParentDate { get; set; }

        //public DateTime? PersonCreationDate { get; set; }

        //public string AddressAdditional { get; set; }

        //public string BankName { get; set; }

        //public string BankBranchNumber { get; set; }

        //public string BankCode { get; set; }

        //public string BankAccount { get; set; }

        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Person>();
            model.ToTable("Persons", "court");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}
