﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;

namespace ST.Net.API.Data.Court
{
    public class Court : BaseModel
    {
        public new Guid Id { get; set; }

        public string Code { get; set; }

        public string Title { get; set; }

        public string Idno { get; set; }

        public string Location { get; set; }

        public string Street { get; set; }

        public string Home { get; set; }

        public string Block { get; set; }

        public string Appartment { get; set; }

        public string Phone { get; set; }

        public string Fax { get; set; }

        public string Email { get; set; }


        public Guid RegionId { get; set; }

        public Region Region { get; set; }




        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Court>();
            model.ToTable("Court", "court");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}