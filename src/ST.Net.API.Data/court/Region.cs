﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;

namespace ST.Net.API.Data.Court
{
    public class Region : BaseModel
    {
        public new Guid Id { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Region>();
            model.ToTable("Regions", "court");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}