﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;

namespace ST.Net.API.Data.Classifier
{
    public class ClassifierParameter : BaseModel
    {
        public ClassifierParameter()
        {
            IsVisible = true;
            AddedAt = DateTime.Now;
            Id = Guid.NewGuid();
        }

        public Classifier Classifier { get; set; }
        [NotMapped]
        public Guid CalssifierId { get; set; }
        
        public string Title { get; set; }
        public int Order { get; set; }
        public bool IsVisible { get; set; }
        public string Comment { get; set; }

        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<ClassifierParameter>();
            model.ToTable("ClassifierParameters", "classifier");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}
