﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;

namespace ST.Net.API.Data.Classifier
{
    public class Classifier : BaseModel
    {
        public Classifier()
        {
            IsVisible = true;
            AddedAt = DateTime.Now;
            Id = Guid.NewGuid();
        }

        public string Code { get; set; }
        public string Title { get; set; }
        public bool IsVisible { get; set; }
        public string Comment { get; set; }

        [NotMapped]
        public ClassifierEnum ClassifierEnum =>
            (ClassifierEnum)Enum.Parse(typeof(ClassifierEnum), Code);

        /// <summary>
        /// Custom ModelDefinition
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder object</param>
        public static void AddModelDefinition(ModelBuilder modelBuilder)
        {
            var model = modelBuilder.Entity<Classifier>();
            model.ToTable("Classifiers", "classifier");

            model.HasKey(u => u.Id);
            model.Property(p => p.Id).ValueGeneratedOnAdd();
        }
    }
}
