﻿using ST.DMS;
using ST.Workflow;
using System;
using System.Collections.Generic;
using System.IO;
namespace ST.Net.API.Data.Case
{
	public class CaseStrategy : IPathGenerator
	{
		public string GeneratePath(string documentName)
		{
			return Path.Combine("Cases", DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(),documentName);
		}
	}
	public class Case : STObject
    {
		public string Name { get; set; }
		public Guid	DocumentId { get; set; }
		public virtual Document Document { get; set; }
		public Guid ClassifierId { get; set; }
		public virtual Classifier.Classifier Classifier { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public bool IsPublic { get; set; }
		public int MaxYears { get; set; }
    }
}
