﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ST.BaseRepository;

namespace ST.Net.API.Core.Controllers.Abstract
{
    /// <summary>
    /// Defines a generic controller for updating entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TUpdateViewModel">List ViewModel</typeparam>
    public interface IUpdateController<TController, TEntity, in TUpdateViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TUpdateViewModel : class
    {
        /// <summary>
        /// Synchronously updates the resource of <see cref="TEntity"/> type.
        /// </summary>
        JsonResult Update(Guid id, [FromBody] TUpdateViewModel model);

        /// <summary>
        /// Asynchronously updates the resource of <see cref="TEntity"/> type.
        /// </summary>
        Task<JsonResult> UpdateAsync(Guid id, [FromBody] TUpdateViewModel model);
    }
}
