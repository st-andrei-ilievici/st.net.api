﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ST.BaseRepository;

namespace ST.Net.API.Core.Controllers.Abstract
{
    /// <summary>
    /// Defines a generic controller for savind entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TRegistreViewModel">Registre ViewModel</typeparam>
    public interface ICreateController<TController, TEntity, in TRegistreViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TRegistreViewModel : class
    {
        /// <summary>
        /// Synchronously creates the resource of <see cref="TEntity"/> type.
        /// </summary>
        JsonResult Create([FromBody] TRegistreViewModel model);

        /// <summary>
        /// Asynchronously creates the resource of <see cref="TEntity"/> type.
        /// </summary>
        Task<JsonResult> CreateAsync([FromBody] TRegistreViewModel model);
    }
}
