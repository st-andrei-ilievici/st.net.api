﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using ST.BaseRepository;

namespace ST.Net.API.Core.Controllers.Abstract
{
    public interface IGetPageController<TController, TEntity, in TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
    {
        IActionResult GetByPage(int page, int itemsPerPage = 10);

        IActionResult GetByPageInclude(int page, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate, int itemsPerPage = 10);
    }
}
