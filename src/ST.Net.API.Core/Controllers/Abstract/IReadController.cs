﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ST.BaseRepository;

namespace ST.Net.API.Core.Controllers.Abstract
{
    /// <summary>
    /// Defines a generic controller for listing entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TListViewModel">List ViewModel</typeparam>
    public interface IReadController<TController, TEntity, in TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TListViewModel : class
    {
        /// <summary>
        /// Synchronously selects the resource of <see cref="TEntity"/> type.
        /// </summary>
        JsonResult Get();

        /// <summary>
        /// Asynchronously selects the resource of <see cref="TEntity"/> type.
        /// </summary>
        Task<JsonResult> GetAsync();

        /// <summary>
        /// Synchronously selects entities of <see cref="TEntity"/> type by page.
        /// </summary>
        JsonResult GetByPage(int page, int pageSize = 10);
        
        /// <summary>
        /// Synchronously select entity of <see cref="TEntity"/> type by ID.
        /// </summary>
        JsonResult GetById(Guid id);

        /// <summary>
        /// Synchronously select entity of <see cref="TEntity"/> type. 
        ///    Get the specified entity including relationship entities. The delegate parameter exposes the <see cref="IQueryable{T}"/>
        /// </summary>
        JsonResult GetByIdInclude(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate);

        /// <summary>
        /// Synchronously selects entities of <see cref="TEntity"/> type. 
        ///    Get the specified page including relationship entities. The delegate parameter exposes the <see cref="IQueryable{T}"/>
        /// </summary>
        JsonResult GetByPageInclude(int page, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate, int pageSize = 10);
    }
}
