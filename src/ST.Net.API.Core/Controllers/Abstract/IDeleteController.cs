﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ST.BaseRepository;

namespace ST.Net.API.Core.Controllers.Abstract
{
    /// <summary>
    /// Defines a generic controller for deleting entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TListViewModel">View model</typeparam>
    public interface IDeleteController<TController, TEntity, in TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TListViewModel : class
    {
        /// <summary>
        /// Synchronously find and delete object by ID parameter.
        /// </summary>
        JsonResult Delete(Guid id);

        /// <summary>
        /// Asynchronously find and delete object by ID parameter.
        /// </summary>
        Task<JsonResult> DeleteAsync(Guid id);
    }
}
