using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AgileObjects.AgileMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Controllers
{
    /// <summary>
    /// Defines a generic controller for updating entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TUpdateViewModel">List ViewModel</typeparam>
    [Produces("application/json")]
    public sealed class UpdateController<TController, TEntity, TUpdateViewModel>
        : Controller, IUpdateController<TController, TEntity, TUpdateViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TUpdateViewModel : class
    {
        private readonly IRepository<DataBaseContext> _repository;

        private readonly ILogger _logger;

        public UpdateController(IRepository<DataBaseContext> repository,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TController>();
            _repository = repository;
        }

        /// <summary>
        /// Synchronously updates the resource of <see cref="TEntity"/> type.
        /// </summary>
        public JsonResult Update(Guid id, [FromBody] TUpdateViewModel model)
        {
            if (model == null)
                return this.ApiBadInputDataResult();

            if (ModelState.IsValid == false)
                return this.ApiBadInputDataResult();

            var entity = _repository.GetSingle<TEntity>(id);
            if (entity == null)
                throw new Exception("Entity not found");

            var mapResult = Mapper.Map(model).Over(entity);
            //Mapper.Map(customerViewModel).OnTo(customer);

            mapResult.ModifiedAt = DateTime.Now;
            _repository.Update(mapResult);

            _logger.LogError(new Logger.Log(entity, "Entity of type " + typeof(TEntity) + " updated.").ToString());

            var result = new SearchResult
            {
                Results = new List<TEntity> { mapResult }
            };

            return this.ApiSuccesResult(result);
        }

        /// <summary>
        /// Asynchronously creates the resource of <see cref="TEntity"/> type.
        /// </summary>
        public Task<JsonResult> UpdateAsync(Guid id, TUpdateViewModel model)
        {
            return Task.FromResult(Update(id, model));
        }
    }
}