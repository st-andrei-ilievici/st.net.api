using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AgileObjects.AgileMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Controllers
{
    /// <summary>
    /// Defines a generic controller for savind entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TListViewModel">View model</typeparam>
    [Produces("application/json")]
    public sealed class DeleteController<TController, TEntity, TListViewModel>
        : Controller, IDeleteController<TController, TEntity, TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TListViewModel : class
    {
        private readonly IRepository<DataBaseContext> _repository;
        private readonly ILogger _logger;

        public DeleteController(IRepository<DataBaseContext> repository,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TController>();
            _repository = repository;
        }

        /// <summary>
        /// Synchronously find and delete object by ID parameter.
        /// </summary>
        public JsonResult Delete(Guid id)
        {
            if (id == Guid.Empty)
                return this.ApiBadInputDataResult();

            var entity = _repository.GetSingle<TEntity>(id);
            if (entity != null)
            {
                entity.IsDeleted = true;
                entity.ModifiedAt = DateTime.Now;

                var guid = _repository.Update(entity);
                if (guid == entity.Id)
                {
                    var viewModel = Mapper.Map(entity).ToANew<TListViewModel>();
                    
                    _logger.LogError(new Logger.Log(entity, "Entity of type " + typeof(TEntity) + " has been deleted.").ToString());
                    var result = new SearchResult
                    {
                        Results = new List<object> { viewModel }
                    };

                    return this.ApiSuccesResult(result);
                }

                _logger.LogWarning(new Logger.Log(entity, "Entity of type " + typeof(TEntity) + " failed to delete.").ToString());

                throw new Exception("Entity could not been deleted.");
            }

            _logger.LogWarning("Entity not found.", id);
            return this.ApiCustomErrorResult(LocalizationMessageCodes.EntityNotFound);
        }

        /// <summary>
        /// Asynchronously find and delete object by ID parameter.
        /// </summary>
        public Task<JsonResult> DeleteAsync(Guid id)
        {
            return Task.FromResult(Delete(id));
        }
    }
}