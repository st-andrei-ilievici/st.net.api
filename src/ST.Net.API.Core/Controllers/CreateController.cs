using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AgileObjects.AgileMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Controllers
{
    /// <summary>
    /// Defines a generic controller for savind entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TRegistreViewModel">Registre ViewModel</typeparam>
    [Produces("application/json")]
    public sealed class CreateController<TController, TEntity, TRegistreViewModel>
        : Controller, ICreateController<TController, TEntity, TRegistreViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TRegistreViewModel : class
    {
        private readonly IRepository<DataBaseContext> _repository;

        private readonly ILogger _logger;

        public CreateController(IRepository<DataBaseContext> repository,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TController>();
            _repository = repository;
        }

        /// <summary>
        /// Synchronously creates the resource of <see cref="TEntity"/> type.
        /// </summary>
        public JsonResult Create([FromBody] TRegistreViewModel model)
        {
            if (model == null)
                return this.ApiBadInputDataResult();

            if (ModelState.IsValid == false)
                return this.ApiBadInputDataResult();

            var entity = Mapper.Map(model).ToANew<TEntity>();
            if (entity == null)
                return this.ApiBadInputDataResult();

            entity.IsDeleted = false;
            entity.AddedAt = DateTime.Now;
            entity.ModifiedById = null;

            var entityId = _repository.Create(entity);
            if (entityId != Guid.Empty)
            {
                entity.Id = entityId;
                var result = new SearchResult
                {
                    Results = new List<TEntity> { entity }
                };

                _logger.LogError(new Logger.Log(entity, "Entity of type " + typeof(TEntity) + " saved.").ToString());
                return this.ApiSuccesResult(result);
            }

            throw new Exception("Entity could not been saved.");
        }

        /// <summary>
        /// Asynchronously creates the resource of <see cref="TEntity"/> type.
        /// </summary>
        public Task<JsonResult> CreateAsync(TRegistreViewModel model)
        {
            return Task.FromResult(Create(model));
        }
    }
}