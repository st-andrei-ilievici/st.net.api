using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Extensions;
using System.Threading.Tasks;
using AgileObjects.AgileMapper;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Controllers
{
    /// <summary>
    /// Defines a generic controller for listing entities of <see cref="TEntity"/> type.
    /// </summary>
    /// <typeparam name="TController">Object of <see cref="Controller"/> type</typeparam>
    /// <typeparam name="TEntity">Entity type</typeparam>
    /// <typeparam name="TListViewModel">List ViewModel</typeparam>
    [Produces("application/json")]
    public sealed class ReadController<TController, TEntity, TListViewModel>
          : Controller, IReadController<TController, TEntity, TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
        where TListViewModel : class
    {
        private readonly IRepository<DataBaseContext> _repository;

        private readonly ILogger _logger;

        /// <summary>
        /// Constructor of <see cref="TEntity"/> controller
        /// </summary>
        public ReadController(IRepository<DataBaseContext> repository,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TController>();
            _repository = repository;
        }

        /// <summary>
        /// Synchronously selects the resource of <see cref="TEntity"/> type.
        /// </summary>
        public JsonResult Get()
        {
            var list = _repository.GetAll<TEntity>();

            var listView = Mapper.Map(list).ToANew<List<TListViewModel>>();

            var total = _repository.Count<TEntity>();

            _logger.LogInformation(new Logger.Log(list).ToString());

            var result = new SearchResult
            {
                Total = total,
                Results = listView
            };

            return this.ApiSuccesResult(result);
        }

        /// <summary>
        /// Asynchronously selects the resource of <see cref="TEntity"/> type.
        /// </summary>
        public Task<JsonResult> GetAsync()
        {
            return Task.FromResult(Get());
        }

        /// <summary>
        /// Synchronously selects entities of <see cref="TEntity"/> type by page.
        /// </summary>
        public JsonResult GetByPage(int page, int pageSize = 10)
        {
            if (page <= 0)
                return this.ApiCustomErrorResult(LocalizationMessageCodes.PageLessThanZero);

            var list = _repository
                .GetPage<TEntity>(page, pageSize)
                .ToList();

            var listView = Mapper.Map(list).ToANew<List<TListViewModel>>();

            var total = _repository.Count<TEntity>();

            _logger.LogInformation(new Logger.Log(list).ToString());

            var result = new SearchResult
            {
                Total = total,
                Page = page,
                PageSize = pageSize,
                Results = (IEnumerable<object>)listView
            };

            return this.ApiSuccesResult(result);
        }

        /// <summary>
        /// Synchronously selects entities of <see cref="TEntity"/> type. 
        ///    Get the specified page including relationship entities. The delegate parameter exposes the <see cref="IQueryable{T}"/>
        /// </summary>
        public JsonResult GetByPageInclude(int page, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate, int pageSize = 10)
        {
            var list = _repository
                .GetPageIncluding(page, includeDelegate, pageSize)
                .ToList();

            var listView = Mapper.Map(list).ToANew<List<TListViewModel>>();

            var total = _repository.Count<TEntity>();

            _logger.LogInformation(new Logger.Log(list).ToString());

            var searchResult = new SearchResult
            {
                Total = total,
                Page = page,
                PageSize = pageSize,
                Results = listView
            };

            return this.ReturnSearchResult(searchResult);
        }

        /// <summary>
        /// Synchronously selects entity of <see cref="TEntity"/> type by ID.
        /// </summary>
        public JsonResult GetById(Guid id)
        {
            var entity = _repository.GetSingle<TEntity>(id);
            if (entity == null)
                return this.ApiCustomErrorResult(LocalizationMessageCodes.EntityNotFound);

            var model = Mapper.Map(entity).ToANew<TListViewModel>();

            _logger.LogInformation(new Logger.Log(model).ToString());

            var result = new SearchResult
            {
                Total = 1,
                Results = new List<TListViewModel> { model }
            };

            return this.ApiSuccesResult(result);
        }

        /// <summary>
        /// Synchronously select entity of <see cref="TEntity"/> type. 
        ///    Get the specified entity including relationship entities. The delegate parameter exposes the <see cref="IQueryable{T}"/>
        /// </summary>
        public JsonResult GetByIdInclude(Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate)
        {
            var entity = _repository.GetSingle<TEntity>(id);
            //var entity = _repository.GetSingleIncluding(id, includeDelegate);
            if (entity == null)
                return this.ApiCustomErrorResult(LocalizationMessageCodes.EntityNotFound);

            var model = Mapper.Map(entity).ToANew<TListViewModel>();

            _logger.LogInformation(new Logger.Log(model).ToString());

            var searchResult = new SearchResult
            {
                Total = 1,
                Page = 1,
                PageSize = 1,
                Results = new List<TListViewModel> { model }
            };

            return this.ReturnSearchResult(searchResult);
        }
    }
}