using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.Net.API.Core.Controllers.Abstract;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Models;
using ST.Net.API.Core.Extensions;
using ST.Net.API.Core.Filters;

namespace ST.Net.API.Core.Controllers
{
    [Produces("application/json")]
    [ServiceFilter(typeof(CustomApiExceptionFilter))]
    public sealed class GetPageIncludeController<TController, TEntity, TListViewModel>
          : Controller, IGetPageIncludeController<TController, TEntity, TListViewModel>
        where TController : Controller
        where TEntity : BaseModel
    {
        private readonly IRepository<DataBaseContext> _repository;

        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public GetPageIncludeController(IRepository<DataBaseContext> repository,
            IMapper mapper,
            ILoggerFactory loggerFactory)
        {
            _mapper = mapper;
            _logger = loggerFactory.CreateLogger<TController>();
            _repository = repository;
        }

        [HttpGet("getpage_include")]
        public IActionResult GetByPageInclude(int page, Func<IQueryable<TEntity>, IQueryable<TEntity>> includeDelegate, int itemsPerPage = 10)
        {
            var list = _repository
                   .GetPageIncluding(page, includeDelegate, itemsPerPage)
                   .ToList();

            var listView = _mapper.Map<List<TListViewModel>>(list);

            var total = _repository.Count<TEntity>();

            _logger.LogInformation(new Logger.Log(list).ToString());

            var result = new SearchResult
            {
                Total = total,
                Page = page,
                PageSize = itemsPerPage,
                Results = (IEnumerable<object>)listView
            };

            return this.ApiSuccesResult(result);
        }
    }
}