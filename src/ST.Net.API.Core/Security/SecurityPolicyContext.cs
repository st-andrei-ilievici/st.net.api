﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using ST.UserIdentity;

namespace ST.Net.API.Core.Security
{
    public sealed class SecurityPolicyContext
    {
        public static List<Permission> Permissions { get; } = new List<Permission>();

        public static void RegisterType<TType>()
            where TType : Controller
        {
            var type = typeof(TType);

            var methods = type.GetMethods();
            foreach (var method in methods)
            {
                var securityAtributes = method.GetCustomAttributes(typeof(SecurityPolicyAttribute), false);
                foreach (var attribute in securityAtributes)
                {
                    var policyAttribute = (SecurityPolicyAttribute)attribute;
                    if (policyAttribute.Arguments == null)
                        continue;

                    var pr = new Permission
                    {
                        Name = string.Format("{0}.{1}", type.Name, method.Name),
                        Category = type.Name
                    };
                    Permissions.Add(pr);
                }
            }
        }
    }
}