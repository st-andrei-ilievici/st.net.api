﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;
using ST.Net.API.Core.Services.Abstract;
using ST.UserIdentity;

namespace ST.Net.API.Core.Security
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SecurityPolicyAttribute : TypeFilterAttribute
    {
        /// <summary>
        /// Initializes a new instance of <see cref="SecurityPolicyAttribute"/>
        /// </summary>
        public SecurityPolicyAttribute()
            : base(typeof(SecurityPolicyHandler))
        {
            Arguments = new string[] { };
        }

        /// <summary>
        /// Executes permission check
        /// </summary>
        private class SecurityPolicyHandler : Attribute, IAsyncResourceFilter
        {
            private readonly JsonSerializerSettings _serializerSettings;

            private readonly UserManager<User> _userManager;

            /// <summary>
            /// Private readonly not nullable IUserRepository implementation.
            /// </summary>
            private readonly IUserService _userService;

            /// <summary>
            /// Public Constructor
            /// </summary>
            public SecurityPolicyHandler(IUserService userService, UserManager<User> userManager)
            {
                _userManager = userManager;
                _userService = userService;

                _serializerSettings = new JsonSerializerSettings
                {
                    Formatting = Formatting.Indented
                };
            }

            /// <summary>
            /// On Resource Executing async method
            /// Throws UnauthorizedResult if permission check is false
            /// </summary>
            /// <param name="context">Resource executing context</param>
            /// <param name="del">Resource executing delegate</param>
            /// <returns>Task</returns>
            public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate del)
            {
                try
                {
                    var actionDescriptor = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)context.ActionDescriptor);
                    if (actionDescriptor == null)
                        throw new UnauthorizedAccessException();

                    var controllerName = actionDescriptor.ControllerName;
                    var actionName = actionDescriptor.ActionName;

                    var identity = context.HttpContext.User.Identity;
                    if (identity == null)
                        throw new UnauthorizedAccessException();

                    if (string.IsNullOrEmpty(identity.Name))
                        throw new UnauthorizedAccessException();

                    var user = await _userManager.FindByEmailAsync(identity.Name);
                    if (user == null)
                        throw new UnauthorizedAccessException();

                    var roles = await _userManager.GetRolesAsync(user);

                    var result = _userService.CheckPermission(user, roles, $"{controllerName}Controller", actionName);
                    if (result == false)
                        throw new UnauthorizedAccessException();

                    await del();
                }
                catch (UnauthorizedAccessException)
                {
                    var result = new ExceptionResultModel
                    {
                        Errors = context.ModelState.Values
                            .SelectMany(x => x.Errors)
                            .Select(x => x.ErrorMessage)
                            .ToList()
                    };

                    result.Errors.Add(LocalizationMessageCodes.MethodNotAllowedBySecurityPolicy);

                    context.Result = new ContentResult
                    {
                        ContentType = "application/json",
                        Content = JsonConvert.SerializeObject(result, _serializerSettings),
                        StatusCode = (int)HttpStatusCode.MethodNotAllowed
                    };

                    await context.Result.ExecuteResultAsync(context);
                }
            }
        }
    }
}