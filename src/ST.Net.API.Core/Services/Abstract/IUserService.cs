﻿using System.Collections.Generic;
using ST.UserIdentity;

namespace ST.Net.API.Core.Services.Abstract
{
    public interface IUserService
    {
        bool CheckPermission(User user, IList<string> roles, string controllerName, string policyAction);
    }
}