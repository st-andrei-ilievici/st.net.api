﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Services.Abstract;
using ST.UserIdentity;

namespace ST.Net.API.Core.Services.Concret
{
    /// <summary>
    /// The service class used to control objects of type <see cref="User"/>.
    ///</summary>
    public sealed class UserService : IUserService
    {
        private readonly IRepository<DataBaseContext> _repository;

        public UserService(IRepository<DataBaseContext> repository)
        {
            _repository = repository;
        }

        public bool CheckPermission(User user, IList<string> roles, string controllerName, string policyAction)
        {
            return true;
            //var result = _repository.GetAllIncluding<RolePermission>(p =>
            //        p.Include(t => t.Permission)
            //            .Include(t => t.Role))
            //    .Where(x => x.Permission.Category == controllerName)
            //    .Where(x => x.Permission.Name == policyAction)
            //    .Where(x => x.IsDeleted == false)
            //    .Any(x => roles.Contains(x.Role.Name));

            //return result;
        }
    }
}