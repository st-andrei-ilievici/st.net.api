﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ST.EntityFramework;
using ST.Net.API.Data.Classifier;
using ST.Net.API.Data.Case;
using ST.Net.API.Data.Complaint;
using ST.Net.API.Data.Court;

namespace ST.Net.API.Core.EntityFramework
{
    /// <summary>
    /// Database Context
    /// </summary>
    public sealed class DataBaseContext : STDbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options">Context Options</param>
        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Defines the object of <see cref="Court"/> type.
        /// TODO Translate: Defineste lista regiunilor RM
        /// </summary>
        public DbSet<Region> Regions { get; set; }

        /// <summary>
        /// Defines the object of <see cref="Court"/> type.
        /// TODO Translate: Defineste lista curtilor si judecatoriilor din sistem.
        /// </summary>
        public DbSet<Court> Courts { get; set; }

        /// <summary>
        /// Defines the object of <see cref="Person"/> type.
        /// TODO Translate: Defineste lista persoanelor inregistrate la cerere/dosar
        /// </summary>
        public DbSet<Person> Persons { get; set; }

        /// <summary>
        /// Defines the object of <see cref="ReportComplaintParty"/> type.
        /// TODO Translate: Defineste lista partilor atasate la cerere
        /// </summary>
        public DbSet<ReportComplaintParty> ComplaintParties { get; set; }

        /// <summary>
        /// Defines the object of <see cref="ReportComplaint"/> type.
        /// TODO Translate: Defineste lista cerilor inregistrate
        /// /// </summary>
        public DbSet<ReportComplaint> ReportComplaints { get; set; }

        ///// <summary>
        ///// Defines the object of <see cref="ReportComplaintPartyType"/> type.
        ///// TODO Translate: Defineste lista regiunilor RM
        ///// </summary>
        //public DbSet<ReportComplaintPartyType> ReportComplaintPartyTypes { get; set; }

		/// <summary>
		/// Defines the object of <see cref="Case"/> type.
		/// </summary>
		public DbSet<Case> Cases { get; set; }


        public DbSet<Classifier> Classifiers { get; set; }
        public DbSet<ClassifierParameter> ClassifierParameters { get; set; }








        /// <summary>
        /// Executed on Model Creating
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .HasOne(d => d.Court)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ReportComplaintParty>()
                .HasOne(d => d.ReportComplaint)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            base.OnModelCreating(modelBuilder);

            Region.AddModelDefinition(modelBuilder);
            Court.AddModelDefinition(modelBuilder);
            Person.AddModelDefinition(modelBuilder);

            Classifier.AddModelDefinition(modelBuilder);
            ClassifierParameter.AddModelDefinition(modelBuilder);
            
            ReportComplaint.AddModelDefinition(modelBuilder);
            ReportComplaintParty.AddModelDefinition(modelBuilder);
        }
    }
}
