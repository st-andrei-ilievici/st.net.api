﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ST.Net.API.Core.Security;
using ST.UserIdentity;
using ST.Workflow;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using ST.Net.API.Data.Complaint;
using ST.Net.API.Data.Court;

namespace ST.Net.API.Core.EntityFramework
{
    public class ContextSeeder
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly DataBaseContext _context;

        public ContextSeeder(IApplicationBuilder app)
        {
            if (app == null)
                throw new ArgumentNullException(nameof(app));

            _context = app.ApplicationServices.GetRequiredService<DataBaseContext>();
            if (_context == null)
                throw new Exception("Database context is missing");

            _roleManager = app.ApplicationServices.GetRequiredService<RoleManager<IdentityRole>>();
            _userManager = app.ApplicationServices.GetRequiredService<UserManager<User>>();
        }

        /// <summary>
        /// Ensure database is created
        /// </summary>
        public async void Process()
        {
            _context.Database.EnsureCreated();

            if (AllMigrationsApplied())
            {
                //Not all database migrations are applied.
                _context.Database.Migrate();
            }

            //Default Administrator Role - Automatically has all permissions
            var admin = new IdentityRole { Name = "Admin" };
            if (await _roleManager.RoleExistsAsync(admin.Name) == false)
                await _roleManager.CreateAsync(admin);

            //Default User Role - Used to define permissions for it's own objects in each Status
            var userRole = new IdentityRole { Name = "User" };
            if (await _roleManager.RoleExistsAsync(userRole.Name) == false)
                await _roleManager.CreateAsync(userRole);

            var user = new User
            {
                Email = "admin@soft-tehnica.com",
                FirstName = "Admin",
                LastName = "Admin",
                FullName = "Admin Admin",
                BirthDate = DateTime.Now,
                UserName = "admin@soft-tehnica.com",
                AddedAt = DateTime.Now,
                PhoneNumber = "+37379087643"
            };

            const string password = "adminSoft-Tehnica";
            if (await _userManager.FindByEmailAsync(user.Email) == null)
            {
                var passwordHash = user.PasswordHash = new PasswordHasher<User>().HashPassword(user, password);
                user.PasswordHash = passwordHash;

                await _userManager.CreateAsync(user);
                await _userManager.AddToRoleAsync(user, admin.Name);
            }

            var permissins = SecurityPolicyContext.Permissions;
            if (permissins.Any())
            {
                foreach (var permission in permissins)
                {
                    if (!_context.Permissions.Any(s => s.Category == permission.Category && s.Name == permission.Name))
                    {
                        _context.Permissions.Add(permission);
                        _context.RolePermissions.Add(new RolePermission { Role = admin, Permission = permission });
                    }
                }
                await _context.SaveChangesAsync();
            }

            var regions = Enumerable.Range(1, 1845)
                .Select(i => new Region
                {
                    Id = Guid.NewGuid(),
                    AddedAt = DateTime.Now,
                    IsDeleted = false,
                    ModifiedAt = DateTime.Now,
                    ModifiedById = "admin",
                    AddedById = Guid.NewGuid().ToString("N"),

                    Name = $"Region {i}"
                })
                .ToList();

            if (!_context.Regions.Any())
            {
                _context.Regions.AddRange(regions);
                _context.SaveChanges();
            }

            var courts = Enumerable.Range(1, 555)
                .Select(i => new Court
                {
                    Id = Guid.NewGuid(),
                    AddedAt = DateTime.Now,
                    IsDeleted = false,
                    ModifiedAt = DateTime.Now,
                    ModifiedById = "admin",
                    AddedById = Guid.NewGuid().ToString("N"),

                    Code = $"Code {i}",
                    Title = $"Title {i}",
                    Idno = string.Format("{0,13:D13}", i),
                    Location = $"Location {i}",
                    Street = $"Street {i}",
                    Home = $"Home {i}",
                    Block = $"Block {i}",
                    Appartment = $"Appartment {i}",
                    Phone = $"Phone {i}",
                    Fax = null,
                    Email = $"Email_{i}@Email.md",
                    Region = regions[new Random(DateTime.Now.Millisecond).Next(0, regions.Count - 1)]
                })
                .ToList();

            if (!_context.Courts.Any())
            {
                _context.Courts.AddRange(courts);
                _context.SaveChanges();
            }

            var persons = Enumerable.Range(1, 9564)
               .Select(i => new Person
               {
                   Id = Guid.NewGuid(),
                   AddedAt = DateTime.Now,
                   IsDeleted = false,
                   ModifiedAt = DateTime.Now,
                   ModifiedById = "admin",
                   AddedById = Guid.NewGuid().ToString("N"),

                   Function = $"Function {i}",
                   LastName = $"LastName {i}",
                   FirstName = $"FirstName {i}",
                   Surname = $"Surname {i}",
                   Idnp = string.Format("{0,13:D13}", i),
                   Gender = $"Gender {i}",
                   Court = courts[new Random(DateTime.Now.Millisecond).Next(0, courts.Count - 1)]
               })
                .ToList();

            if (!_context.Persons.Any())
            {
                _context.Persons.AddRange(persons);
                _context.SaveChanges();
            }

            var workFlow = new Workflow.Workflow
            {
                Name = "ReportComplaint_Workflow",
                Description = "ReportComplaint Description",
                ObjectsName = "Project"
            };
			var workflow2 = new Workflow.Workflow
			{
				Name = "CasesWorkflow",
				ObjectsName = "Case"
			};
            _context.Workflows.AddRange(workFlow, workflow2);
			_context.DocumentConfigurations.Add(new DMS.DocumentConfiguration { ServerPath = @"C:\Users\User\Desktop" });
			_context.Classifiers.Add(new Data.Classifier.Classifier {Code = "Art.", Title = "Furt di piceniki", Comment = "Furtu de piceniki se pedepseste cu moartea", IsVisible = true});
            var initial = new Status { Name = "Initial", Workflow = workFlow, Deadline = null, };
            var approved = new Status { Name = "Approved", Workflow = workFlow, Deadline = new TimeSpan(0, 0, 30, 0).Ticks };
            var archived = new Status { Name = "Archived", Workflow = workFlow, IsFinal = true, Deadline = null };
            var deleted = new Status { Name = "Deleted", Workflow = workFlow, Deadline = null };
			var initial2 = new Status { Name = "Initial", Workflow = workflow2 };
            _context.Statuses.AddRange(initial, approved, archived, deleted,initial2);

            var reportComplaints = Enumerable.Range(1, 5000)
               .Select(i => new ReportComplaint
               {
                   Id = Guid.NewGuid(),
                   AddedAt = DateTime.Now,
                   IsDeleted = false,
                   ModifiedAt = DateTime.Now,
                   ModifiedById = "admin",
                   AddedById = Guid.NewGuid().ToString("N"),

                   Title = $"Title {i}",
                   Annex = i,
                   AutomaticCode = Guid.NewGuid(),
                   ManualCode = $"ManualCode {i}",
                   Comments = null,
                   Created = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                   Court = courts[new Random(DateTime.Now.Millisecond).Next(0, courts.Count - 1)],
                   Status = initial,
                   Workflow = workFlow
               })
                .ToList();

            if (!_context.ReportComplaints.Any())
            {
                _context.ReportComplaints.AddRange(reportComplaints);
                _context.SaveChanges();
            }


            var listPartyTypes = new List<string>
            {
                "Pîrît",
                "Autoritate publică",
                "Reprezentant legal",
                "Revizuient - Participant",
                "Avocat",
                "Reprezentant",
                "Persoană interesată",
                "Intervenient principal",
                "Copîrît",
                "Executor",
                "Fidejusor",
                "Executor judecătoresc",
                "Petiţionar",
                "Altul",
                "Intervenient accesoriu",
                "Coreclamant",
                "Procuror",
                "Debitor",
                "Creditor",
                "Reclamant",
                "Petiţionar",
                "Expert"
            };


            var complaintParties = Enumerable.Range(1, 5000)
               .Select(i => new ReportComplaintParty
               {
                   Id = Guid.NewGuid(),
                   AddedAt = DateTime.Now,
                   IsDeleted = false,
                   ModifiedAt = DateTime.Now,
                   ModifiedById = "admin",
                   AddedById = Guid.NewGuid().ToString("N"),

                   PartyComments = $"PartyComments {i}",
                   PartyDateAdd = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                   PartyDateEnd = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),

                   Person = persons[new Random(DateTime.Now.Millisecond).Next(0, persons.Count - 1)],
                   ReportComplaint = reportComplaints[new Random(DateTime.Now.Millisecond).Next(0, reportComplaints.Count - 1)]
               })
                .ToList();

            if (!_context.ComplaintParties.Any())
            {
                _context.ComplaintParties.AddRange(complaintParties);
                _context.SaveChanges();
            }

            var workflow = new Workflow.Workflow { Name = "ReportComplaintWorkflow", ObjectsName = nameof(ReportComplaint) };
            //context.Workflows.Add(workflow);
            var status = new Status { Name = "Initial", Workflow = workflow };
            //context.Statuses.Add(status);
            var reportComlaints = Enumerable.Range(1, 5000)
                .Select(x => new ReportComplaint
                {
                    Workflow = workflow,
                    Status = status,
                    Title = "title",
                    CaseLinkId = Guid.NewGuid(),
                    CaseKindId = Guid.NewGuid(),
                    AutomaticCode = Guid.NewGuid(),
                    ManualCode = Guid.NewGuid().ToString(),
                    TypeExamId = Guid.NewGuid(),
                    SubTypeId = Guid.NewGuid(),
                    IsSecret = true,
                    LinkReportComplaint = "youtube.com",
                    IsSameJudge = false,
                    RequestFormId = Guid.NewGuid(),
                    CaseCateogryId = Guid.NewGuid(),
                    Nature = "priroda",
                    Volumes = 5,
                    Annex = 2,
                    StateFee = 22.5f,
                    StateFeeReasonId = Guid.NewGuid(),
                    Comments = "zis iz a camentarii",
                    CourtId = Guid.NewGuid(),
                    CurentState = "palajenie situatii",
                    Created = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    OldCaseLinkId = Guid.NewGuid(),
                    IsResolved = false,
                    CancellationDate = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    CancellationComment = "judecatorul o luatm mită",
                    CancellationAuthor = "valera",
                    CancellationParticipantId = Guid.NewGuid(),
                    SendCourt = "Costiujeni",
                    SendDate = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    SendAuthor = "Acelasi Valera",
                    SendComments = "a pintru și?",
                    ReportIsActive = false,
                    CourtDateTo = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    TransferedDate = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    ArchivedOn = new DateTime(1900, 1, 1).AddDays(new Random().Next((DateTime.Today - new DateTime(1900, 1, 1)).Days)),
                    Judge_Id = Guid.NewGuid(),
                    Main_ReportComplaintId = Guid.NewGuid(),
                    NotBeAssesed = true,
                    ActionValue = 42,
                    SubTypeChecked = false,
                    CaseCategoryChecked = true
                }).ToList();

            if (!_context.ReportComplaints.Any())
            {
                await _context.ReportComplaints.AddRangeAsync(reportComlaints);
                await _context.SaveChangesAsync();
            }
        }

        private bool AllMigrationsApplied()
        {
            var applied = _context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = _context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }
    }
}
