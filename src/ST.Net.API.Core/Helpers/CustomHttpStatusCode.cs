﻿namespace ST.Net.API.Core.Helpers
{
    public enum CustomHttpStatusCode
    {
        //422 - Unprocessable Entity (validation error)
        UnprocessableEntity = 422
    }
}