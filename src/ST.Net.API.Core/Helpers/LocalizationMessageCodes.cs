﻿namespace ST.Net.API.Core.Helpers
{
    /// <summary>
    /// API Error Codes user by Front-End Layer for localization
    /// </summary>
    public static class LocalizationMessageCodes
    {
        /// <summary>
        /// The Email field is required.
        /// </summary>
        public const string EmailRequired = "API_EmailRequired";

        /// <summary>
        /// The Name field is required.
        /// </summary>
        public const string NameRequired = "API_NameRequired";

        /// <summary>
        /// The Title field is required.
        /// </summary>
        public const string TitleRequired = "API_TitleRequired";
		/// <summary>
		/// Id is required
		/// </summary>
		public const string IdRequired = "API_IdRequired";
        



        /// <summary>
        /// The Email field is not a valid e-mail address.
        /// </summary>
        public const string EmailIsNotValid = "API_EmailIsNotValid";

        /// <summary>
        /// The Password field is required.
        /// </summary>
        public const string PasswordRequired = "API_PasswordRequired";

        /// <summary>
        /// The Confirm password field is required.
        /// </summary>
        public const string ConfirmPasswordRequired = "API_ConfirmPasswordRequired";

        /// <summary>
        /// The Confirm password field is required.
        /// </summary>
        public const string ConfirmPasswordDoNotMatch = "API_ConfirmPasswordDoNotMatch";

        /// <summary>
        /// Bad input data
        /// </summary>
        public const string BadInputData = "API_BadInputData";

        /// <summary>
        /// User is locked out
        /// </summary>
        public const string UserIsLockedOut = "API_UserIsLockedOut";

        /// <summary>
        /// Authentication is not allowed
        /// </summary>
        public const string AuthenticationIsNotAllowed = "API_AuthenticationIsNotAllowed";

        /// <summary>
        /// Bad request
        /// </summary>
        public const string BadRequest = "API_BadRequest";

        /// <summary>
        /// User has been not found
        /// </summary>
        public const string UserNotFoundByEmail = "API_UserNotFoundByEmail";

        ///// <summary>
        ///// The user name that already exists.
        ///// </summary>
        //public const string DuplicateUserName = "API_DuplicateUserName";

        /// <summary>
        /// The FirstName field is required.
        /// </summary>
        public const string FirstNameRequired = "API_FirstNameRequired";

        /// <summary>
        /// The LastName field is required.
        /// </summary>
        public const string LastNameRequired = "API_LastNameRequired";

        /// <summary>
        /// The Birthday field is required.
        /// </summary>
        public const string BirthdayRequired = "API_BirthdayRequired";

        /// <summary>
        /// The PhoneNumber field is required.
        /// </summary>
        public const string PhoneNumberRequired = "API_PhoneNumberRequired";

        /// <summary>
        /// The FullName field is required.
        /// </summary>
        public const string FullNameRequired = "API_FullNameRequired";

        /// <summary>
        /// Page cannot be less or equal to 0
        /// </summary>
        public static string PageLessThanZero = "API_PageLessThanZero";


        /// <summary>
        /// Invalid reset password token
        /// </summary>
        public static string InvalidResetPasswordToken = "API_PageLessThanZero";

        /// <summary>
        /// Unauthorized
        /// </summary>
        public static string Unauthorized = "API_Unauthorized";

        /// <summary>
        /// Unhandled Exception
        /// </summary>
        public static string UnhandledException = "API_UnhandledException";

        /// <summary>
        /// Wrong Password
        /// </summary>
        public static string WrongPassword = "API_WrongPassword";


        /// <summary>
        /// Bag token provided
        /// </summary>
        public static string BagTokenProvided = "API_BagTokenProvided";


        /// <summary>
        /// Entity not found
        /// </summary>
        public const string EntityNotFound = "API_EntityNotFound";




        /// <summary>
        ///Method is not allowed according to system permissions
        /// </summary>
        public const string MethodNotAllowedBySecurityPolicy = "API_MethodNotAllowedBySecurityPolicy";


        /// <summary>
        ///Token expired
        /// </summary>
        public const string TokenExpired = "API_TokenExpired";

		/// <summary>
		/// Document is required
		/// </summary>
		public const string DocumentRequired = "API_DocumentRequired";
		/// <summary>
		/// Entity could not be saved
		/// </summary>
		public const string SaveError = "API_SaveError";
		/// <summary>
		/// Entity could not be updated
		/// </summary>
		public const string UpdateError = "API_UpdateError";
		/// <summary>
		/// Entity could not be deleted
		/// </summary>
		public const string DeleteError = "API_DeleteError";
		/// <summary>
		/// Entities could not be read
		/// </summary>
		public const string ReadError = "API_ReadError";
		/// <summary>
		/// File already exists exception
		/// </summary>
		public const string FileAlreadyExists = "API_FileAlreadyExists";
		/// <summary>
		/// File is required
		/// </summary>
		public const string FileRequired = "API_FileRequired";
		/// <summary>
		/// Required for common cases
		/// </summary>
		public const string Required = "API_Required";
		/// <summary>
		/// File not found exception
		/// </summary>
		public const string FileNotFound = "API_FileNotFound";
    }
}
