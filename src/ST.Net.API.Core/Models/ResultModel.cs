﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace ST.Net.API.Core.Models
{
    public class SuccesResultModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SuccesResultModel()
        {
            IsSucces = true;
            Result = null;
        }

        /// <summary>
        /// Bool indicating that the request resulted with success.
        /// </summary>
        [DefaultValue(true)]
        [JsonProperty("success")]
        public bool IsSucces { get; set; }

        /// <summary>
        /// The result of the response, if there is no errors.
        /// </summary>
        [JsonProperty("data")]
        public SearchResult Result { get; set; }
    }

    public class ExceptionResultModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ExceptionResultModel()
        {
            Errors = new List<string>();
        }

        /// <summary>
        /// Bool indicating that the request resulted with success.
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty("success")]
        public bool IsSucces { get; set; } = false;

        /// <summary>
        /// This property will contain errors if any.
        /// </summary>
        [JsonProperty("localization_error_codes")]
        public IList<string> Errors { get; set; }
    }
}
