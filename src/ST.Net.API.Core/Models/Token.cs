﻿using System;
using Newtonsoft.Json;

namespace ST.Net.API.Core.Models
{
    public class Token
    {
        [JsonProperty("token_type")]
        public string Type { get; set; }

        [JsonProperty("token_access")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public int Expires { get; set; }

        [JsonProperty("timestamp")]
        public DateTime TimeStamp => DateTime.Now;
    }
}