﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ST.Net.API.Core.Models
{
    public class ApiResultModel
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ApiResultModel()
        {
            ErrorKeys = new List<string>();
            Result = new SearchResult();
        }

        /// <summary>
        /// Bool indicating that the request resulted with an
        /// error. If True than the <see cref="ErrorKeys"/> will 
        /// contain a Error message that produced this error.
        /// </summary>
        [JsonProperty("is_error")]
        public bool IsError { get; set; }
        
        /// <summary>
        /// Bool indicating that the request resulted with success.
        /// </summary>
        [JsonProperty("is_success")]
        public bool IsSucces { get; set; }

        /// <summary>
        /// This property will contain error keys if any.
        /// </summary>
        [JsonProperty("error_keys")]
        public IList<string> ErrorKeys { get; set; }

        /// <summary>
        /// The result of the response, if there is no errors.
        /// </summary>
        [JsonProperty("result")]
        public object Result { get; set; }
    }
}
