﻿using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace ST.Net.API.Core.Models
{
    public class SearchResult
    {
        [DefaultValue(1)]
        [JsonProperty("total_count")]
        public long Total { get; set; }

        [DefaultValue(1)]
        [JsonProperty("page")]
        public int Page { get; set; }

        [DefaultValue(1)]
        [JsonProperty("page_size")]
        public int PageSize { get; set; }

        [JsonProperty("total_pages")]
        public long TotalPages
        {
            //TODO: cand vei ajunge sa faci paginare sa tii cont cand calculezi numarul total de pagini sa folosesti Math.Ceiliing(totalPages/pageSize);
            get
            {
                if (PageSize == 0)
                    return 0;

                var pages = Total / PageSize;
                if (Total % PageSize > 0)
                    pages++;

                return pages;
            }
        }

        //[JsonProperty("elapsed_milliseconds")]
        //public long ElapsedMilliseconds { get; set; }

        [JsonProperty("result")]
        public IEnumerable<object> Results { get; set; }
    }
}