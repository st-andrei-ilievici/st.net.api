﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Filters
{
    /// <summary>
    /// Fileter of <see cref="IActionFilter"/> type used to validate Controller MVC Model State
    /// </summary>
    public sealed class ModelValidationFilter : IActionFilter
    {
        private readonly JsonSerializerSettings _serializerSettings;

        public ModelValidationFilter()
        {
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Trigger the filter OnActionExecuting
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid)
                return;

            if (context.HttpContext.Request.Method.Equals("GET"))
            {
                context.HttpContext.Response.ContentType = "application/json";
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                context.Result = new BadRequestResult();
            }
            else
            {
                var result = new ExceptionResultModel
                {
                    Errors = context.ModelState.Values
                        .SelectMany(x => x.Errors)
                        .Select(x => x.ErrorMessage)
                        .ToList()
                };

                context.Result = new ContentResult
                {
                    ContentType = "application/json",
                    Content = JsonConvert.SerializeObject(result, _serializerSettings),
                    StatusCode = (int)CustomHttpStatusCode.UnprocessableEntity
                };
            }
        }

        /// <summary>
        /// Trigger the filter OnActionExecuted
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }
    }
}