﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Middlewares
{
    /// <summary>
    /// API Exception Handler Middleware
    /// </summary>
    public sealed class ErrorHandlingMiddleware
    {
        private readonly JsonSerializerSettings _serializerSettings;

        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        /// <summary>
        /// Invoke Middleware method
        /// </summary>
        /// <param name="context">Http httpContext</param>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// Exception middleware handling method
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        private Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var result = new ExceptionResultModel
            {
                Errors = new List<string>
                {
                    LocalizationMessageCodes.UnhandledException
                },
            };

            #region Check Exception Type

            if (exception is SecurityTokenExpiredException)
            {
                result.Errors.Add(LocalizationMessageCodes.TokenExpired);
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }
            else
            if (exception is UnauthorizedAccessException)
            {
                result.Errors.Add(LocalizationMessageCodes.Unauthorized);
                result.Errors.Add(LocalizationMessageCodes.MethodNotAllowedBySecurityPolicy);
                httpContext.Response.StatusCode = (int)HttpStatusCode.MethodNotAllowed;
            }
            else
            {
                if (!string.IsNullOrEmpty(exception.Message))
                    result.Errors.Add(exception.Message);
            }

            #endregion

            var serializeObject = JsonConvert.SerializeObject(result, _serializerSettings);
            
            return httpContext.Response.WriteAsync(serializeObject);
        }
    }
}
