﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ST.Net.API.Core.Helpers;
using ST.Net.API.Core.Models;

namespace ST.Net.API.Core.Extensions
{
    /// <summary>
    /// Controller extension
    /// </summary>
    public static class ControllerExtentions
    {
        /// <summary>
        /// JSON serializer settings
        /// </summary>
        private static JsonSerializerSettings SerializerSettings =>
            new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };

        /// <summary>
        /// Equivalent to HTTP status 200. Extension method thar return serialized JSON object of <see cref="ApiResultModel"/> type,
        ///    indicates that request succeeded
        /// </summary>
        /// <param name="controller">Given controller</param>
        /// <param name="result">Obtional parameter of <see cref="SearchResult"/> type</param>
        /// <returns><see cref="JsonResult"/> object</returns>
        public static JsonResult ApiSuccesResult(this Controller controller, object result = null)
        {
            return controller.Json(
                new ApiResultModel
                {
                    IsSucces = true,
                    IsError = false,
                    Result = result ?? new object(),
                }, SerializerSettings);
        }

        /// <summary>
        /// Equivalent to HTTP status 412. Extension method thar return serialized JSON object of <see cref="ApiResultModel"/> type, indicates that a condition set for this request failed
        /// </summary>
        /// <param name="controller">Given controller</param>
        /// <returns><see cref="JsonResult"/> object</returns>
        public static JsonResult ApiBadInputDataResult(this Controller controller)
        {
            return controller.Json(new ApiResultModel
            {
                IsSucces = false,
                IsError = true,
                ErrorKeys = new[] { LocalizationMessageCodes.BadInputData },
                Result = new object()
            }, SerializerSettings);
        }

        /// <summary>
        /// Equivalent to HTTP status 500. Extension method thar return serialized JSON object of <see cref="ApiResultModel"/> type,
        ///    indicates that a generic error has occurred on the server
        /// </summary>
        /// <param name="controller">Given controller</param>
        /// <returns><see cref="JsonResult"/> object</returns>
        public static JsonResult ApiErrorsResult(this Controller controller)
        {
            return controller.Json(new ApiResultModel
            {
                IsSucces = false,
                IsError = true,
                ErrorKeys = controller.ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage)
                    .ToList(),
                Result = new object()
            }, SerializerSettings);
        }

        /// <summary>
        /// Extension method thar return serialized JSON object of <see cref="ApiResultModel"/> type, with custom error key
        /// </summary>
        /// <param name="controller">Given controller</param>
        /// <param name="errorKey">Given error key. Recommended to use keys of <see cref="LocalizationMessageCodes"/> object (for better localization experience)</param>
        /// <returns></returns>
        public static JsonResult ApiCustomErrorResult(this Controller controller, string errorKey)
        {

            return controller.Json(new ApiResultModel
            {
                IsSucces = false,
                IsError = true,
                ErrorKeys = new[] { errorKey },
                Result = new object()
            }, SerializerSettings);
        }

        public static string GetAuthorizedUserEmail(this Controller controller)
        {
            var userName = controller.User.FindFirst(ClaimTypes.NameIdentifier);

            if (userName == null)
                throw new UnauthorizedAccessException();

            if (string.IsNullOrEmpty(userName.Value))
                throw new UnauthorizedAccessException();

            return userName.Value;
        }


        public static JsonResult ReturnEntityNotFound(this Controller controller)
        {
            controller.HttpContext.Response.ContentType = "application/json";
            controller.HttpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
            var result = new ExceptionResultModel
            {
                IsSucces = false,
                Errors = new List<string>
                {
                    LocalizationMessageCodes.EntityNotFound
                }
            };
            return controller.Json(result, SerializerSettings);
        }

        public static JsonResult ReturnSearchResult(this Controller controller, SearchResult searchResult)
        {
            controller.HttpContext.Response.ContentType = "application/json";
            controller.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK;
            var result = new SuccesResultModel
            {
                IsSucces = true,
                Result = searchResult
            };
            return controller.Json(result, SerializerSettings);
        }


        public static JsonResult ReturnBadRequest(this Controller controller, IdentityResult identyResult = null)
        {
            controller.HttpContext.Response.ContentType = "application/json";
            controller.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            var result = new ExceptionResultModel
            {
                IsSucces = false,
                Errors = new List<string>
                {
                    LocalizationMessageCodes.BadRequest
                }
            };

            if (identyResult != null)
            {
                var errors = identyResult.Errors
                    .Select(x => $"API_{x.Code} = {x.Description}")
                    .ToList();

                foreach (var error in errors)
                {
                    result.Errors.Add(error);
                }
            }

            return controller.Json(result, SerializerSettings);
        }

        public static JsonResult ReturnBadRequest(this Controller controller, string errorKey)
        {
            controller.HttpContext.Response.ContentType = "application/json";
            controller.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            var result = new ExceptionResultModel
            {
                IsSucces = false,
                Errors = new List<string>
                {
                    LocalizationMessageCodes.BadRequest
                }
            };

            if (!string.IsNullOrEmpty(errorKey))
                result.Errors.Add(errorKey);

            return controller.Json(result, SerializerSettings);
        }




        //public static JsonResult GetErrors<T>(this Controller controller, JsonSerializerSettings serializerSettings)
        //{
        //    return controller.Json(new ApiResultModel
        //    {
        //        IsError = true,
        //        ErrorKeys = controller.ModelState.Values
        //            .SelectMany(x => x.Errors)
        //            .Select(x => x.ErrorMessage)
        //    }, serializerSettings);
        //}

        public static JsonResult BadRequest<T>(this Controller controller, JsonSerializerSettings serializerSettings)
        {
            return controller.Json(new ApiResultModel
            {
                IsError = true,
                ErrorKeys = new[] { LocalizationMessageCodes.BadRequest },
            }, serializerSettings);
        }

        //public static JsonResult ReturnBadInputData(this Controller controller, JsonSerializerSettings serializerSettings)
        //{
        //    return controller.Json(new ApiResultModel
        //    {
        //        IsError = true,
        //        ErrorKeys = new[] { LocalizationMessageCodes.BadInputData },
        //    }, serializerSettings);
        //}

        public static JsonResult ReturnFail<T>(this Controller controller, JsonSerializerSettings serializerSettings)
        {
            return controller.Json(new ApiResultModel
            {
                IsError = true,
                ErrorKeys = new[] { "Fail" }
            }, serializerSettings);
        }



        public static JsonResult ReturnException(this Controller controller, Exception exception, JsonSerializerSettings serializerSettings)
        {
            //TODO Exception: de modificat (necesar de precautat exceptia)
            return controller.Json(new ApiResultModel
            {
                IsError = true,
                ErrorKeys = new[] { "Exception" },
            }, serializerSettings);
        }
    }
}
