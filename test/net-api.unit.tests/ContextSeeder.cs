﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ST.Net.API.Core.EntityFramework;
using ST.UserIdentity;
using System.Threading.Tasks;


namespace ST.Net.API.UnitTests
{
    internal static class ContextSeeder
    {
        public static async Task Seed(this DataBaseContext context, RoleManager<IdentityRole> roleManager,
            UserManager<User> userManager)
        {
            var roleAdmin = new IdentityRole { Name = "Admin" };
            if (await roleManager.RoleExistsAsync(roleAdmin.Name) == false)
                await roleManager.CreateAsync(roleAdmin);

            var user = new User
            {
                Email = "admin@test.test",
                FirstName = "Admin",
                LastName = "Admin",
                FullName = "Admin Admin",
                BirthDate = DateTime.Now,
                AddedAt = DateTime.Now,
                PhoneNumber = ""
            };

            if (await userManager.FindByEmailAsync(user.Email) == null)
            {
                user.UserName = user.Email;
                var password = "test-password";
                user.PasswordHash = user.PasswordHash = new PasswordHasher<User>().HashPassword(user, password);

                await userManager.CreateAsync(user);
                await userManager.AddToRoleAsync(user, roleAdmin.Name);
            }

            await context.SaveChangesAsync();
        }
    }
}
