﻿using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ST.BaseRepository;
using ST.EntityFramework;
using ST.Net.API.Core.EntityFramework;
using ST.UserIdentity;
using Xunit;

namespace ST.Net.API.UnitTests.controllers
{
    public class PersonsControllerTests
    {
        //private readonly PersonsController _controller;

        public PersonsControllerTests()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddEntityFrameworkSqlServer()
                .AddDbContext<DataBaseContext>(opts => { opts.UseInMemoryDatabase("net.api.testing"); });

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<STDbContext>()
                .AddDefaultTokenProviders();

            //Repositories
            services.AddTransient<IRepository<DataBaseContext>, Repository<DataBaseContext>>();

            var serviceProvider = services.BuildServiceProvider();
            var app = new ApplicationBuilder(serviceProvider);
            app.Build();

            serviceProvider = app.ApplicationServices;

            //var repository = serviceProvider.GetRequiredService<Repository<DataBaseContext>>();
            //var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();
         
            //_controller = new PersonsController(repository, mapper, loggerFactory);
        }
        
        /// <summary>
        /// Test constructor
        /// </summary>
        [Fact]
        public void Constructor()
        {
            //Assert.NotNull(_controller);
        }

        //[Fact]
        //public void GetFirstPage()
        //{
        //    var result = _controller.GetByPage(1);

        //    Assert.IsType<JsonResult>(result);
        //}
    }
}
