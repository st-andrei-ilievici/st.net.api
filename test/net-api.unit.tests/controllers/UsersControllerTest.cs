﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Builder.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ST.Net.API.Controllers;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Core.Services.Abstract;
using ST.Net.API.Core.Services.Concret;
using ST.Net.API.Models.Users;
using ST.UserIdentity;
using Xunit;

namespace ST.Net.API.UnitTests.controllers
{
    public class UsersControllerTest
    {
       // private readonly UsersController _controller;
        private readonly DataBaseContext _context;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<User> _signInManager;
        private readonly HttpContext _httpContext;

        public UsersControllerTest()
        {
            IServiceCollection services = new ServiceCollection();

            services.AddEntityFrameworkSqlServer()
                .AddDbContext<DataBaseContext>(opts => { opts.UseInMemoryDatabase("net.api.testing"); });

            services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<DataBaseContext>()
                .AddDefaultTokenProviders();

            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
            

            _httpContext = new DefaultHttpContext();
            _httpContext.Features.Set<IHttpAuthenticationFeature>(new HttpAuthenticationFeature());
            services.AddSingleton<IHttpContextAccessor>(h => new HttpContextAccessor
            {
                HttpContext = _httpContext
            });

            var serviceProvider = services.BuildServiceProvider();
            var app = new ApplicationBuilder(serviceProvider);
            app.UseIdentity();

            app.Build();

            serviceProvider = app.ApplicationServices;

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            _context = serviceProvider.GetRequiredService<DataBaseContext>();

            _userManager = serviceProvider.GetRequiredService<UserManager<User>>();
            _roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            _signInManager = serviceProvider.GetRequiredService<SignInManager<User>>();

            if (_context.Database.EnsureCreated())
                _context.Seed(_roleManager, _userManager).Wait();

            var user = _userManager.FindByEmailAsync("admin@test.test").Result;
            _httpContext.User = _signInManager.CreateUserPrincipalAsync(user).Result;

            //_controller = new UsersController(loggerFactory, _userManager, _roleManager, _tokenService)
            //{
            //    ControllerContext = new ControllerContext
            //    {
            //        HttpContext = _httpContext
            //    },
            //    TempData = new TempDataDictionary(_httpContext, serviceProvider.GetRequiredService<ITempDataProvider>())
            //};
        }

        /// <summary>
        /// Test constructor
        /// </summary>
        [Fact]
        public void Test_Constructor()
        {
          //  Assert.NotNull(_controller);
            Assert.NotNull(_httpContext);
            Assert.NotNull(_userManager);
            Assert.NotNull(_roleManager);
            Assert.NotNull(_signInManager);
            Assert.NotNull(_context);
        }

        //[Fact]
        //public async void Test_Login_Fail_UserNotFoundByEmail()
        //{
        //    var model = new LoginViewModel
        //    {
        //        Password = "no pass",
        //        Email = "incorrect email regex"
        //    };
        //    var result = await _controller.Login(model);

        //    Assert.NotNull(result);
        //    Assert.IsType<ApiResultModel>(result.Value);

        //    var resultModel = result.Value as ApiResultModel;
        //    Assert.NotNull(resultModel);
        //    Assert.Equal(true, resultModel.IsError);
        //    Assert.NotNull(resultModel.ErrorKeys);
        //    Assert.Equal(true, resultModel.ErrorKeys.Contains(LocalizationMessageCodes.UserNotFoundByEmail));
        //}

        [Fact]
        public void Test_Register_Registre()
        {
            var model = new RegisterViewModel
            {
                FirstName = "test",
                LastName = "test",
                BirthDate = new DateTime(2000, 1, 1),
                Password = "100de100#RP",
                ConfirmPassword = "100de100#RP",
                Email = "test@test.com",
                PhoneNumber = "+37369457907"
            };

            //var result = _controller.Register(model);
            //var okResult = Assert.IsType<OkResult>(result);
            //Assert.NotNull(okResult);
        }
    }
}