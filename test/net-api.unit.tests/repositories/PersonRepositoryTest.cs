﻿using System;
using Microsoft.EntityFrameworkCore;
using ST.BaseRepository;
using ST.Net.API.Core.EntityFramework;
using ST.Net.API.Data.court;
using Xunit;

namespace ST.Net.API.UnitTests.repositories
{
    public class PersonRepositoryTest
    {
        [Fact]
        public void Add_Person()
        {
            var options = new DbContextOptionsBuilder<DataBaseContext>()
                .UseInMemoryDatabase("person.db.test")
                .Options;

            using (var context = new DataBaseContext(options))
            {
                var court = new Court
                {
                    Id = Guid.NewGuid(),
                    AddedAt = DateTime.Now,
                    IsDeleted = false,
                    ModifiedAt = DateTime.Now,
                    ModifiedById = "test_user",
                    AddedById = Guid.NewGuid().ToString("N")
                };

                var person = new Person
                {
                    Id = Guid.NewGuid(),
                    AddedAt = DateTime.Now,
                    IsDeleted = false,
                    ModifiedAt = DateTime.Now,
                    ModifiedById = "test_user",
                    AddedById = Guid.NewGuid().ToString("N"),
                    Court = court
                };

                context.Courts.Add(court);
                context.Persons.Add(person);

                context.SaveChanges();

                var repository = new Repository<DataBaseContext>(context);

                var valC = repository.GetSingle<Court>(s => s.Id == court.Id);

                var valP = repository.GetSingle<Person>(s => s.Id == person.Id);

                Assert.NotNull(valC);

                Assert.NotNull(valP);

                Assert.True(valP.CourtId == valC.Id);
            }
        }
    }
}