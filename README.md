## Changing DB
To use another database:
- Edit `ConnectionStrings` in _appsettings.json_
- Edit in _Startup.cs_:
```C#
services.AddDbContext<DataBaseContext>(
    opts => opts.UseSqlServer(Configuration.GetConnectionString("api.db.context"),
        b => b.MigrationsAssembly("net.api")));
```

## Swagger
- Navigate to `http://localhost:50642/swagger/swagger.json` to see the document generated that describes the endpoints
- Swagger UI can be viewed by navigating to `http://localhost:50642/swagger`

To test the APIs, remove the policy from controllers.

## License